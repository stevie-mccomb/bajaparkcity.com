<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/**
 * Public
 */
Route::view('/', 'index')->name('home');
Route::redirect('/menus/dinner', '/menus/menu', 302);
Route::get('menus/dirty-sodas', 'MenuController@dirtySodas')->name('menus.dirty-sodas');
Route::get('menus/{slug}', 'MenuController@show')->name('menus.show');
Route::get('hours', 'HourController@show')->name('hours');

Route::view('reviews', 'reviews.create')->name('reviews.create');
Route::post('reviews', 'ReviewController@submitNegativeReview')->name('reviews.store');
Route::view('reviews/thank-you', 'reviews.thank-you')->name('reviews.thank-you');

Route::view('contact', 'contact.form')->name('contact');
Route::post('contact', 'ContactController@send')->name('contact.send');
Route::view('contact/thank-you', 'contact.thank-you')->name('contact.thank-you');

/**
 * Auth
 */
require __DIR__ . '/auth.php';

/**
 * Cantina Club
 */
Route::group(['prefix' => 'cantina-club', 'middleware' => 'auth'], function () {
    Route::get('profile', 'ProfileController@edit')->name('cantina-club.profile');
    Route::patch('profile', 'ProfileController@update')->name('cantina-club.profile.update');

    Route::group(['middleware' => 'verified'], function () {
        Route::get('/', 'DashboardController@dashboard')->name('cantina-club');

        Route::get('points', 'PointController@index')->name('cantina-club.points');
        Route::get('verified-actions/{verifiedAction}/visit', 'PointController@visitVerifiedAction')->name('cantina-club.verified-actions.visit');

        Route::get('coupons', 'CouponController@index')->name('cantina-club.coupons');
    });
});

/**
 * Admin
 */
Route::group(['prefix' => 'admin', 'middleware' => ['auth', 'can:view-admin'], 'as' => 'admin.'], function () {
    Route::redirect('/', '/admin/menus')->name('dashboard');

    Route::resource('menus', 'MenuController', [
        'except' => ['show']
    ]);

    Route::group(['middleware' => 'can:update-hours'], function () {
        Route::get('hours/edit', 'HourController@edit')->name('hours.edit');
        Route::patch('hours', 'HourController@update')->name('hours.update');
    });

    Route::get('point-adjustments', 'Admin\PointAdjustmentController@index')->name('point-adjustments.index');
    Route::get('point-adjustments/create', 'Admin\PointAdjustmentController@create')->name('point-adjustments.create');
    Route::post('point-adjustments', 'Admin\PointAdjustmentController@store')->name('point-adjustments.store');
    Route::get('point-adjustments/users/{user}/create-from-qr-code', 'Admin\PointAdjustmentController@createFromQrCode')->name('point-adjustments.create-from-qr-code');
    Route::post('point-adjustments/users/{user}/store-from-qr-code', 'Admin\PointAdjustmentController@storeFromQrCode')->name('point-adjustments.store-from-qr-code');

    Route::group([
        'middleware' => 'can:manage-coupons',
        'prefix' => 'coupons',
        'as' => 'coupons.',
        'namespace' => 'Admin',
        'controller' => 'CouponController',
    ], function () {
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::post('/', 'store')->name('store');
        Route::get('{coupon}/edit', 'edit')->name('edit');
        Route::patch('{coupon}', 'update')->name('update');
        Route::delete('{coupon}', 'destroy')->name('delete');
        Route::get('{coupon}/redeem', 'getRedeem')->name('redeem');
        Route::post('{coupon}/redeem', 'postRedeem')->name('redeem.post');
        Route::get('{coupon}/send', 'getSend')->name('send');
        Route::post('{coupon}/send', 'postSend')->name('send.post');
    });

    Route::group([
        'prefix' => 'verified-actions',
        'as' => 'verified-actions.',
        'namespace' => 'Admin',
        'controller' => 'VerifiedActionController'
    ], function () {
        Route::get('/', 'index')->name('index');
        Route::middleware('can:verify-actions')->get('{verifiedAction}/verify', 'getVerify')->name('verify');
        Route::middleware('can:verify-actions')->post('{verifiedAction}/verify', 'postVerify')->name('verify.post');

        Route::group(['middleware' => 'can:manage-verified-actions'], function () {
            Route::get('create', 'create')->name('create');
            Route::post('/', 'store')->name('store');
            Route::get('{verifiedAction}/edit', 'edit')->name('edit');
            Route::patch('{verifiedAction}', 'update')->name('update');
            Route::delete('{verifiedAction}', 'destroy')->name('delete');
        });
    });

    Route::middleware(['can:manage-users'])->resource('users', 'UserController', [
        'except' => ['show']
    ]);

    /**
     * Audit Log
     */
    Route::group(['middleware' => 'can:view-audit-log', 'prefix' => 'audit-log', 'as' => 'audit-log.', 'namespace' => 'Admin'], function () {
        Route::get('/', 'AuditLogController@index')->name('index');
        Route::get('events/{auditLogEvent}/show', 'AuditLogController@show')->name('events.show');
    });
});

/**
 * Async
 */
Route::group(['prefix' => 'async', 'as' => 'async.'], function () {
    Route::get('menus/{menu}/content', 'MenuController@asyncContent')->name('menus.content');
    Route::get('members', 'UserController@asyncIndexCantinaClubMembers')->name('members.index');
});

/**
 * Redirects
 */
Route::redirect('home', '/', 301);
