export const generateId = function(length = 8) {
    let id = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz';

    while (id.length < length) {
        id += characters[Math.floor(Math.random() * characters.length)];
    }

    return id;
};

export default () => ({ generateId });
