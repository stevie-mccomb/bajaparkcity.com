import '@/bootstrap';

import DirtySodaMenu from '@/components/DirtySodaMenu.vue';
import Editor from '@/components/Editor.vue';
import MenuEditor from '@/components/MenuEditor.vue';
import RandomBackground from '@/components/RandomBackground.vue';
import Recaptcha from '@/components/Recaptcha.vue';
import ResourceFilter from '@/components/ResourceFilter.vue';
import Review from '@/components/Review.vue';
import Slug from '@/components/Slug.vue';
import SpendPoints from '@/components/SpendPoints.vue';
import Tooltip from '@/components/Tooltip.vue';

import { createApp } from 'vue';

const app = createApp({
    components: {
        'dirty-soda-menu': DirtySodaMenu,
        'editor': Editor,
        'menu-editor': MenuEditor,
        'random-background': RandomBackground,
        'recaptcha': Recaptcha,
        'resource-filter': ResourceFilter,
        'review': Review,
        'slug': Slug,
        'spend-points': SpendPoints,
        'tooltip': Tooltip,
    }
});

app.mount('#app');
