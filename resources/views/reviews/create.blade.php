@extends('layouts.app')

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>
                Leave a Review
            </h1>
        </div>
    </div>

    <div class="review-form padded wrapper">
        @include('partials.alerts')

        <Review :old="{{ json_encode(old()) }}" />
    </div>
@endsection
