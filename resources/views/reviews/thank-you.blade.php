@extends('layouts.app')

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>
                Thank you!
            </h1>
        </div>
    </div>

    <div class="review-thank-you padded wrapper">
        <p>We will review your feedback and be in touch if more details are needed.</p>
    </div>
@endsection
