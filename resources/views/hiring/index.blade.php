@extends('layouts.app')

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>We're Hiring!</h1>
        </div>
    </div>

    <div class="padded wrapper">
        <p>We're currently hiring for the Winter season! Come on in and drop off an application or give us a call at <a href="tel:4356492252">(435) 649-2252</a> for more information.</p>
    </div>
@endsection
