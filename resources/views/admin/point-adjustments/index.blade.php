@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Point Adjustments
        </h1>

        <a href="{{ route('admin.point-adjustments.create') }}">
            <button class="button" type="button">
                Create Point Adjustment
            </button>
        </a>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if ($pointAdjustments->count())
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>Member</th>

                    <th>Adjuster</th>

                    <th>Value</th>

                    <th>Reason</th>

                    <th>When</th>
                </tr>

                @foreach ($pointAdjustments as $pointAdjustment)
                    <tr>
                        <td>
                            {{ $pointAdjustment->user->name }}
                        </td>

                        <td>
                            {{ $pointAdjustment->adjuster->name ?? 'System' }}
                        </td>

                        <td>
                            {{ $pointAdjustment->value }}
                        </td>

                        <td>
                            {{ $pointAdjustment->reason }}
                        </td>

                        <td>
                            {{ $pointAdjustment->created_at->format('F jS, Y') }}
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $pointAdjustments->appends(request()->all())->links() }}
    @else
        <p>No point adjustments found.</p>
    @endif
@endsection
