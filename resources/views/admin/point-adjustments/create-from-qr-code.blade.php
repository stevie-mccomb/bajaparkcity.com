@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Confirm Pico Point Spend
        </h1>
    </header>

    <form action="{{ route('admin.point-adjustments.store-from-qr-code', $member) }}" method="POST">
        @csrf

        @include('partials.alerts')

        <fieldset>
            <legend>Confirm Details of Transaction</legend>

            <div class="form-group required">
                <label for="member">Member</label>

                <input id="member" type="text" readonly value="{{ $member->name }} &lt;{{ $member->email }}&gt;">
            </div>

            <div class="form-group required">
                <label for="value">Points to Spend</label>

                <input id="value" name="value" type="number" readonly value="{{ $value }}">
            </div>

            <div class="form-group required">
                <label for="dollar_value">Dollar Value</label>

                <input id="dollar_value" type="text" readonly value="{{ pointsToDollars($value) }}">
            </div>
        </fieldset>

        <button class="button" type="submit">
            Confirm Transaction
        </button>

        <a href="{{ route('admin.point-adjustments.index') }}" class="outline button" style="margin-left: 1rem;">
            Decline Transaction
        </a>
    </form>
@endsection
