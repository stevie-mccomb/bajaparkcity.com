@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Create Point Adjustment
        </h1>
    </header>

    <form action="{{ route('admin.point-adjustments.store') }}" method="POST">
        @csrf

        @include('partials.alerts')

        <fieldset>
            <legend>Member</legend>

            <div class="form-group required">
                <label for="member">Member</label>

                <resource-filter
                    id="member"
                    name="member"
                    load-url="{{ route('async.members.index') }}"
                    :model-value="{{ json_encode(!empty(old('member')) ? [(int) old('member')] : []) }}"
                    :multiple="false"
                ></resource-filter>
            </div>
        </fieldset>

        <fieldset>
            <legend>Adjustment</legend>

            <div class="form-group required">
                <label for="value">Value</label>

                <input id="value" name="value" type="number" required min="-9999" max="9999" step="1" value="{{ old('value') }}">
            </div>

            <div class="form-group required">
                <label for="reason">Reason</label>

                <textarea id="reason" name="reason" rows="3" required max="255">{{ old('reason') }}</textarea>
            </div>

            <p class="form-help">
                The "Reason" field can be seen by the customer and by restaurant ownership. Please be respectful and courteous.
            </p>
        </fieldset>

        <button class="button" type="submit">
            Save Point Adjustment
        </button>
    </form>
@endsection
