<nav class="admin-sidebar">
    @can('manage-menus')
        <a href="{{ route('admin.menus.index') }}" class="{{ ActiveRoute::startsWith('admin/menus') }}">
            Menus
        </a>
    @endcan

    @can('manage-hours')
        <a href="{{ route('admin.hours.edit') }}" class="{{ ActiveRoute::startsWith('admin/hours/edit') }}">
            Hours
        </a>
    @endcan

    @can('manage-points')
        <a href="{{ route('admin.point-adjustments.index') }}" class="{{ ActiveRoute::startsWith('admin.point-adjustments.index') }}">
            Point Adjustments
        </a>
    @endcan

    @can('manage-coupons')
        <a href="{{ route('admin.coupons.index') }}" class="{{ ActiveRoute::startsWith('admin.coupons.index') }}">
            Coupons
        </a>
    @endcan

    @can('manage-verified-actions')
        <a href="{{ route('admin.verified-actions.index') }}" class="{{ ActiveRoute::startsWith('admin.verified-actions.index') }}">
            Verified Actions
        </a>
    @endcan

    @can('manage-users')
        <a href="{{ route('admin.users.index') }}" class="{{ ActiveRoute::startsWith('admin/users') }}">
            Users
        </a>
    @endcan

    @can('view-audit-log')
        <a href="{{ route('admin.audit-log.index') }}" class="{{ ActiveRoute::startsWith('admin.audit-log.index') }}">
            Audit Log
        </a>
    @endcan

    <form action="{{ route('logout') }}" method="POST">
        @csrf

        <a href="#" onclick="this.parentNode.submit();">
            Log Out
        </a>
    </form>
</nav>
