@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>Menus</h1>

        <a href="{{ route('admin.menus.create') }}">
            <button class="button" type="button">
                Create Menu
            </button>
        </a>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if (count($menus))
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>Name</th>

                    <th class="icon">View</th>

                    <th class="icon">Edit</th>

                    <th class="icon">Delete</th>
                </tr>

                @foreach ($menus as $menu)
                    <tr>
                        <td>
                            <a href="{{ route('admin.menus.edit', $menu) }}">
                                {{ $menu->name }}
                            </a>
                        </td>

                        <td class="icon">
                            <a target="_blank" href="{{ route('menus.show', [ 'slug' => $menu ]) }}">
                                <span class="fal fa-lg fa-eye"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.menus.edit', $menu) }}">
                                <span class="fal fa-lg fa-edit"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <form action="{{ route('admin.menus.destroy', $menu) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to delete the {{ $menu->name }} menu?')) return false;">
                                @csrf @method('DELETE')

                                <button type="submit">
                                    <span class="fal fa-lg fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>There are currently no menus.</p>
    @endif
@endsection
