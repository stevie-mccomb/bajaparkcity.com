@extends('layouts.admin')

@section('content')
    <h1>{{ $menu->id ? 'Edit' : 'Create' }} Menu</h1>

    <hr>

    <form action="{{ $menu->id ? route('admin.menus.update', $menu) : route('admin.menus.store') }}" method="POST">
        @csrf @method($menu->id ? 'PATCH' : 'POST')

        @include('partials.alerts')

        <div class="form-group required">
            <label for="name">Name</label>

            <input id="name" name="name" type="text" value="{{ old('name') ? old('name') : $menu->name }}">
        </div>

        <div class="form-group required">
            <label for="slug">Slug</label>

            <slug value="{{ old('slug') ? old('slug') : $menu->slug }}"></slug>
        </div>

        <div class="form-group required">
            <label for="sort_order">Sort Order</label>

            <input id="sort_order" name="sort_order" type="text" value="{{ old('sort_order') ? old('sort_order') : $menu->sort_order }}">
        </div>

        <menu-editor name="content" :content="{{ json_encode($menu->content) }}"></menu-editor>

        <div class="form-group">
            <button class="button" type="submit">
                Save Menu
            </button>
        </div>
    </form>
@endsection
