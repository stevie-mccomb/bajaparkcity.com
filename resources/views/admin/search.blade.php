<form class="admin-search" method="GET">
    <input id="search" name="search" type="search" value="{{ request()->get('search') }}">

    <button type="submit">
        <span class="far fa-search"></span>
    </button>
</form>
