@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>Edit Hours</h1>
    </header>

    <form action="{{ route('admin.hours.update') }}" method="POST">
        @csrf @method('PATCH')

        @include('partials.alerts')

        <div class="form-group required">
            <label for="hours">Hours</label>

            <editor id="hours" name="hours" :value="{{ $hours ?? '' }}"></editor>
        </div>

        <button class="button" type="submit">
            Save Hours
        </button>
    </form>
@endsection
