@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>Users</h1>

        <a href="{{ route('admin.users.create') }}">
            <button class="button" type="button">
                Create User
            </button>
        </a>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if ($users->count())
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>
                        First Name
                    </th>

                    <th>
                        Last Name
                    </th>

                    <th>
                        Email
                    </th>

                    <th class="icon">
                        Edit
                    </th>

                    <th class="icon">
                        Delete
                    </th>
                </tr>

                @foreach ($users as $user)
                    <tr>
                        <td>
                            <a href="{{ route('admin.users.edit', $user) }}">
                                {{ $user->first_name }}
                            </a>
                        </td>

                        <td>
                            <a href="{{ route('admin.users.edit', $user) }}">
                                {{ $user->last_name }}
                            </a>
                        </td>

                        <td>
                            <a href="{{ route('admin.users.edit', $user) }}">
                                {{ $user->email }}
                            </a>
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.users.edit', $user) }}">
                                <span class="fal fa-lg fa-edit"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <form action="{{ route('admin.users.destroy', $user) }}" method="POST">
                                @csrf @method('DELETE')

                                <button type="submit">
                                    <span class="fal fa-lg fa-trash"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $users->appends(request()->all())->links() }}
    @else
        <p>There are currently no users.</p>
    @endif
@endsection
