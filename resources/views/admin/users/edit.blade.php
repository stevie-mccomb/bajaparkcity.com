@extends('layouts.admin')

@section('content')
    <h1>{{ $user->id ? 'Edit' : 'Create' }} User</h1>

    <hr>

    <form action="{{ $user->id ? route('admin.users.update', $user) : route('admin.users.store') }}" method="POST">
        @csrf @method($user->id ? 'PATCH' : 'POST')

        @include('partials.alerts')

        @if ($roles->count())
            <div class="form-group required">
                <label for="role_id">Role</label>

                @php
                    $oldRole = old('role', $user->role_id) ?? 0;
                @endphp

                <select id="role_id" name="role_id">
                    <option value="">Guest</option>

                    @foreach ($roles as $role)
                        <option value="{{ $role->id }}" {{ $oldRole === $role->id ? 'selected' : '' }}>
                            {{ $role->name }}
                        </option>
                    @endforeach
                </select>
            </div>
        @endif

        <div class="form-row">
            <div class="form-group required">
                <label for="first_name">First Name</label>

                <input id="first_name" name="first_name" type="text" value="{{ old('first_name') ?? $user->first_name }}">
            </div>

            <div class="form-group required">
                <label for="last_name">Last Name</label>

                <input id="last_name" name="last_name" type="text" value="{{ old('last_name') ?? $user->last_name }}">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group required">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email') ?? $user->email }}">
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>

                <input id="phone" name="phone" type="phone" value="{{ old('phone') ?? $user->phone }}">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group {{ $user->id ? '' : 'required' }}">
                <label for="new_password">
                    {{ $user->id ? 'New' : '' }} Password
                </label>

                <input id="new_password" name="new_password" type="password" autocomplete="new-password">
            </div>

            <div class="form-group {{ $user->id ? '' : 'required' }}">
                <label for="new_password_confirmation">
                    Confirm {{ $user->id ? 'New' : '' }} Password
                </label>

                <input id="new_password_confirmation" name="new_password_confirmation" type="password" autocomplete="new-password">
            </div>
        </div>

        <button class="button" type="submit">
            Save User
        </button>
    </form>
@endsection
