@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Coupons
        </h1>

        <a href="{{ route('admin.coupons.create') }}">
            <button class="button" type="button">
                Create Coupon
            </button>
        </a>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if ($coupons->count())
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>Name</th>

                    <th>Code</th>

                    <th>Description</th>

                    <th class="icon">Redeem</th>

                    <th class="icon">Send</th>

                    <th class="icon">Edit</th>

                    <th class="icon">Archive</th>
                </tr>

                @foreach ($coupons as $coupon)
                    <tr>
                        <td>
                            <a href="{{ route('admin.coupons.edit', $coupon) }}">
                                {{ $coupon->name }}
                            </a>
                        </td>

                        <td>
                            {{ $coupon->code }}
                        </td>

                        <td>
                            {{ $coupon->description }}
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.coupons.redeem', $coupon) }}" title="Redeem">
                                <span class="fal fa-lg fa-ticket"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.coupons.send', $coupon) }}" title="Send to Member">
                                <span class="fal fa-lg fa-paper-plane"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.coupons.edit', $coupon) }}" title="Edit">
                                <span class="fal fa-lg fa-edit"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <form action="{{ route('admin.coupons.delete', $coupon) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to archive the \'{{ $coupon->name }}\' coupon?')) return false;" title="Archive">
                                @csrf @method('DELETE')

                                <button type="submit">
                                    <span class="fal fa-lg fa-archive"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>No coupons found.</p>
    @endif
@endsection
