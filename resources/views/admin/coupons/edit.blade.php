@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            {{ $coupon->id ? 'Edit' : 'Create' }} Coupon
        </h1>
    </header>

    <form action="{{ $coupon->id ? route('admin.coupons.update', $coupon) : route('admin.coupons.store') }}" method="POST">
        @csrf @method($coupon->id ? 'PATCH' : 'POST')

        @include('partials.alerts')

        <fieldset>
            <legend>Details</legend>

            <div class="form-group required">
                <label for="name">Name</label>

                <input id="name" name="name" type="text" value="{{ old('name', $coupon->name) }}">
            </div>

            <div class="form-group">
                <label for="code">Code</label>

                <input id="code" name="code" type="text" value="{{ old('code', $coupon->code) }}">

                <p class="form-help">
                    While not required to create a coupon, this field is required if you want customers to redeem this coupon directly through online takeout orders. A corresponding "offer" with a matching code must be available in the Toast POS system for this to function.
                </p>
            </div>

            <div class="form-group required">
                <label for="description">Description</label>

                <textarea id="description" name="description" required max="255">{{ old('description', $coupon->description) }}</textarea>
            </div>

            <div class="form-group">
                <label for="terms">Terms</label>

                <textarea id="terms" name="terms" max="255">{{ old('terms', $coupon->terms) }}</textarea>

                <p class="form-help">
                    This field is for providing special terms and conditions that may apply to the coupon to ensure it's usage is not abused. Useful for making exceptions for specific menu items, days of the week, times of day, order composition, etc. Try not to use this field to hide things, but rather to ensure the restaurant is protected from abuse. If an exception on this list may not be expected or common, include it in the description.
                </p>
            </div>
        </fieldset>

        <fieldset>
            <legend>Availability</legend>

            <div class="form-row">
                <div class="form-group">
                    <label for="active_at">Active At</label>

                    <input id="active_at" name="active_at" type="datetime-local" step="1" value="{{ old('active_at', $coupon->active_at ? $coupon->active_at->format('Y-m-d\TH:i:s') : '') }}">
                </div>

                <div class="form-group required">
                    <label for="expires_at">Expires At</label>

                    <input id="expires_at" name="expires_at" type="datetime-local" required step="1" value="{{ old('expires_at', $coupon->expires_at ? $coupon->expires_at->format('Y-m-d\TH:i:s') : '') }}">
                </div>
            </div>

            <div class="form-group">
                <label for="public">Public</label>

                <select id="public" name="public">
                    <option value="0" {{ old('public', $coupon->public) == 0 ? 'selected' : '' }}>No</option>

                    <option value="1" {{ old('public', $coupon->public) == 1 ? 'selected' : '' }}>Yes</option>
                </select>

                <p class="form-help">
                    If set to 'Yes', this coupon will be automatically made available to all members of the Cantina Club.
                </p>
            </div>
        </fieldset>

        <button class="button" type="submit">
            Save Coupon
        </button>
    </form>
@endsection
