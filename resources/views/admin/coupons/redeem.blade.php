@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Redeem Coupon
        </h1>
    </header>

    <form action="{{ route('admin.coupons.redeem.post', $coupon) }}" method="POST">
        @csrf

        @include('partials.alerts')

        <div class="form-group required">
            <label for="members">Cantina Club Members</label>

            <resource-filter
                id="members"
                name="members[]"
                load-url="{{ route('async.members.index') }}"
                :model-value="{{ json_encode(old('members', [])) }}"
            ></resource-filter>
        </div>

        <button class="button" type="submit">
            Redeem Coupon
        </button>
    </form>
@endsection
