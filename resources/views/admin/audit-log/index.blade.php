@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Audit Log
        </h1>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if ($auditLogEvents->count())
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>Event</th>

                    <th>User</th>

                    <th>Summary</th>

                    <th>Has Data?</th>

                    <th class="icon">View</th>
                </tr>

                @foreach ($auditLogEvents as $auditLogEvent)
                    <tr>
                        <td>
                            <a href="{{ route('admin.coupons.edit', $auditLogEvent) }}">
                                {{ $auditLogEvent->event }}
                            </a>
                        </td>

                        <td>
                            {{ $auditLogEvent->user->name }}
                        </td>

                        <td>
                            {{ $auditLogEvent->summary }}
                        </td>

                        <td>
                            {{ !empty($auditLogEvent->data) ? 'Yes' : 'No' }}
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.audit-log.events.show', $auditLogEvent) }}" title="View Event">
                                <span class="fal fa-lg fa-eye"></span>
                            </a>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    @else
        <p>No events found.</p>
    @endif
@endsection
