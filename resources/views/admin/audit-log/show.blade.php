@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            View Audit Event
        </h1>
    </header>

    <fieldset>
        <legend>Details</legend>

        <div class="form-group">
            <label for="event">Event</label>

            <input id="event" type="text" readonly value="{{ $auditLogEvent->event }}">
        </div>

        <div class="form-group">
            <label for="summary">Summary</label>

            <textarea id="summary" rows="5" readonly>{{ $auditLogEvent->summary }}</textarea>
        </div>

        <div class="form-group">
            <label for="when">When</label>

            <input id="when" type="text" readonly value="{{ $auditLogEvent->created_at->format('F jS, Y @ H:i:s T') }}">
        </div>
    </fieldset>

    <fieldset>
        <legend>Affected Resource</legend>

        <div class="form-group">
            <label for="resource">Resource</label>

            <textarea id="resource" rows="16" readonly>
Type: {{ substr($auditLogEvent->auditable_type, strrpos($auditLogEvent->auditable_type, '\\') + 1) }}

{{ $auditLogEvent->auditable->toJson(JSON_PRETTY_PRINT) }}
            </textarea>
        </div>
    </fieldset>

    <fieldset>
        <legend>User Info</legend>

        <div class="form-group">
            <label for="user">User</label>

            <textarea id="user" rows="12" readonly>{{ $auditLogEvent->user->toJson(JSON_PRETTY_PRINT) }}</textarea>
        </div>
    </fieldset>

    <fieldset>
        <legend>Data</legend>

        <div class="form-group">
            <label for="data">Data</label>

            <textarea id="data" rows="16" readonly>{{ json_encode(json_decode($auditLogEvent->data), JSON_PRETTY_PRINT) }}</textarea>
        </div>
    </fieldset>
@endsection
