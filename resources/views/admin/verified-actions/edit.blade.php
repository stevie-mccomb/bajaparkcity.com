@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            {{ $verifiedAction->id ? 'Edit' : 'Create' }} Verified Action
        </h1>
    </header>

    <form action="{{ $verifiedAction->id ? route('admin.verified-actions.update', $verifiedAction)  : route('admin.verified-actions.store') }}" method="POST">
        @csrf @method($verifiedAction->id ? 'PATCH' : 'POST')

        @include('partials.alerts')

        <fieldset>
            <legend>Details</legend>

            <div class="form-group required">
                <label for="name">Name</label>

                <input id="name" name="name" type="text" required max="255" value="{{ old('name', $verifiedAction->name) }}">
            </div>

            <div class="form-group required">
                <label for="description">Description</label>

                <textarea id="description" name="description" rows="3" required max="255">{{ old('description', $verifiedAction->description) }}</textarea>
            </div>

            <div class="form-group">
                <label for="url">URL</label>

                <input id="url" name="url" type="url" max="255" value="{{ old('url', $verifiedAction->url) }}">
            </div>
        </fieldset>

        <fieldset>
            <legend>Settings</legend>

            <div class="form-group required">
                <label for="value">Value</label>

                <input id="value" name="value" type="number" required min="1" max="100" value="{{ old('value', $verifiedAction->value) }}">

                <p class="form-help">
                    This is how many points the member will earn if they complete this action.
                </p>
            </div>

            <div class="form-group">
                <label for="automatic">Automatic?</label>

                <select id="automatic" name="automatic">
                    <option value="0" {{ old('automatic', $verifiedAction->automatic) == 0 ? 'selected' : '' }}>No</option>

                    <option value="1" {{ old('automatic', $verifiedAction->automatic) == 1 ? 'selected' : '' }}>Yes</option>
                </select>

                <p class="form-help">
                    If set to "Yes", this action will automatically complete itself as soon as the member click the button.
                </p>
            </div>
        </fieldset>

        <button class="button" type="submit">
            Save Verified Action
        </button>
    </form>
@endsection
