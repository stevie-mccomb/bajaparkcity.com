@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Verify Action
        </h1>
    </header>

    <form action="{{ route('admin.verified-actions.verify.post', $verifiedAction) }}" method="POST">
        @csrf

        @include('partials.alerts')

        <fieldset>
            <legend>Members</legend>

            <div class="form-group required">
                <label for="members">Members</label>

                <resource-filter
                    id="members"
                    name="members[]"
                    load-url="{{ route('async.members.index') }}"
                    :model-value="{{ json_encode(old('members', [])) }}"
                ></resource-filter>
            </div>
        </fieldset>

        <button class="button" type="submit">
            Verify Action
        </button>
    </form>
@endsection
