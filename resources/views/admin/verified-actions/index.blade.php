@extends('layouts.admin')

@section('content')
    <header class="admin-header">
        <h1>
            Verified Actions
        </h1>

        <a href="{{ route('admin.verified-actions.create') }}">
            <button class="button" type="button">
                Create Verified Action
            </button>
        </a>

        @include('admin.search')
    </header>

    @include('partials.alerts')

    @if ($verifiedActions->count())
        <table class="resource-listing">
            <tbody>
                <tr>
                    <th>Name</th>

                    <th>Description</th>

                    <th>Value</th>

                    <th>Automatic</th>

                    <th>Members</th>

                    <th class="icon">Verify</th>

                    <th class="icon">Edit</th>

                    <th class="icon">Archive</th>
                </tr>

                @foreach ($verifiedActions as $verifiedAction)
                    <tr>
                        <td>
                            {{ $verifiedAction->name }}
                        </td>

                        <td>
                            {{ $verifiedAction->description }}
                        </td>

                        <td class="icon">
                            {{ $verifiedAction->value }}
                        </td>

                        <td class="icon">
                            <span class="{{ $verifiedAction->automatic ? 'fal fa-check' : '' }}"></span>
                        </td>

                        <td class="icon">
                            {{ $verifiedAction->users_count }}
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.verified-actions.verify', $verifiedAction) }}" title="Verify" aria-label="Verify">
                                <span class="fal fa-lg fa-check-circle"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <a href="{{ route('admin.verified-actions.edit', $verifiedAction) }}" title="Edit" aria-label="Edit">
                                <span class="fal fa-lg fa-edit"></span>
                            </a>
                        </td>

                        <td class="icon">
                            <form action="{{ route('admin.verified-actions.delete', $verifiedAction) }}" method="POST" onsubmit="if (!confirm('Are you sure you want to archive the \'{{ $verifiedAction->name }}\' verified action?')) return false;" title="Archive" aria-label="Archive">
                                @csrf @method('DELETE')

                                <button type="submit">
                                    <span class="fal fa-lg fa-archive"></span>
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>

        {{ $verifiedActions->appends(request()->all())->links() }}
    @else
        <p>No verified actions found.</p>
    @endif
@endsection
