@extends('layouts.app')

@section('content')
    <form class="auth-form" method="POST">
        @csrf

        <header>
            <h1>
                Cantina Club
            </h1>
        </header>

        <section>
            @include('partials.alerts')

            <div class="form-group">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email') }}">
            </div>

            <div class="form-group">
                <label for="password">Password</label>

                <input id="password" name="password" type="password">
            </div>

            <button class="button" type="submit">
                Log In
            </button>
        </section>

        <footer>
            <a href="{{ route('register') }}">
                Not a member?
            </a>

            <a href="{{ route('password.email') }}">
                Forgot your password?
            </a>
        </footer>
    </form>
@endsection
