@extends('layouts.app')

@section('content')
    <form class="auth-form" action="{{ route('password.update') }}" method="POST">
        @csrf

        <input type="hidden" name="token" value="{{ $request->token }}">

        <header>
            <h1>Reset Your Password</h1>
        </header>

        <section>
            @include('partials.alerts')

            <div class="form-group">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email') ?? $request->email }}">
            </div>

            <div class="form-group">
                <label for="password">New Password</label>

                <input id="password" name="password" type="password">
            </div>

            <div class="form-group">
                <label for="password_confirmation">Confirm New Password</label>

                <input id="password_confirmation" name="password_confirmation" type="password">
            </div>
        </section>

        <footer>
            <button class="button" type="submit">
                Reset Password
            </button>
        </footer>
    </form>
@endsection
