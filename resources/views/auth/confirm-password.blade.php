@extends('layouts.app')

@section('content')
    <form class="auth-form" method="POST">
        @csrf

        <header>
            <h1>
                Confirm Password
            </h1>
        </header>

        <section>
            @include('partials.alerts')

            <div class="form-group">
                <label for="password">Password</label>

                <input id="password" name="password" type="password">
            </div>

            <button class="button" type="submit">
                Confirm Password
            </button>
        </section>
    </form>
@endsection
