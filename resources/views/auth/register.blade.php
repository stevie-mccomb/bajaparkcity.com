@extends('layouts.app')

@section('content')
    <recaptcha action="contact" g-recaptcha-key="{{ config('services.google.recaptcha.key') }}">
        <form class="auth-form" method="POST">
            @csrf

            <header>
                <h1>
                    Join the Cantina Club
                </h1>
            </header>

            <section>
                @include('partials.alerts')

                <div class="alert info">
                    <h2>Want free stuff?</h2>

                    <p>Who doesn't? Join the <strong>Cantina Club</strong> for free and get regular discounts and offers sent straight to your email. Or, if you'd prefer not to receive emails, you can opt-out and regularly check your dashboard for active offers instead.</p>
                </div>

                <div class="form-row">
                    <div class="form-group required">
                        <label for="first_name">First Name</label>

                        <input id="first_name" name="first_name" type="text" value="{{ old('first_name') }}" required autofocus autocomplete="first-name" max="255">
                    </div>

                    <div class="form-group required">
                        <label for="last_name">Last Name</label>

                        <input id="last_name" name="last_name" type="text" value="{{ old('last_name') }}" required autocomplete="last-name" max="255">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group required">
                        <label for="email">Email</label>

                        <input id="email" name="email" type="email" value="{{ old('email') }}" required max="255">
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone</label>

                        <input id="phone" name="phone" type="tel" value="{{ old('phone') }}">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group">
                        <label for="password">Password</label>

                        <input id="password" name="password" type="password">
                    </div>

                    <div class="form-group">
                        <label for="password_confirmation">Confirm Password</label>

                        <input id="password_confirmation" name="password_confirmation" type="password">
                    </div>
                </div>

                <button class="button" type="submit">
                    <span class="fal fa-party-horn"></span> Join the Club
                </button>
            </section>

            <footer>
                <a href="{{ route('login') }}">
                    Already have an account?
                </a>
            </footer>
        </form>
    </recaptcha>
@endsection
