@push('styles')
    <style type="text/css">
        .notification-form {
            display: inline;
        }
    </style>
@endpush

<x-cantina-club class="dashboard">
    <x-slot:title>
        Welcome to the Cantina Club!
    </x-slot>

    <p>
        It looks like your email address has not been verified yet. You should receive an email soon with a button you can click to verify. If you do not receive the email within 15 minutes, <a class="notification-anchor" href="#" onclick="event.preventDefault(); document.querySelector('.notification-form').submit();">click here to resend the email</a>.
    </p>

    <form class="notification-form" action="{{ route('verification.send') }}" method="POST">@csrf</form>
</x-cantina-club>
