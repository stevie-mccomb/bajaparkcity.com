@extends('layouts.app')

@section('content')
    <form class="auth-form" method="POST">
        @csrf

        <header>
            <h1>
                Forgot Password
            </h1>
        </header>

        <section>
            @include('partials.alerts')

            <div class="form-group">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email') }}">
            </div>

            <button class="button" type="submit">
                Send Password Reset Link
            </button>
        </section>
    </form>
@endsection
