    </div> <!-- End of #app -->

    <script src="https://kit.fontawesome.com/c97cb1efe4.js" crossorigin="anonymous" async></script>
    @vite('resources/js/app.js')
    @stack('scripts')
</body>
</html>
