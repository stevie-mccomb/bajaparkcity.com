@if (isset($errors) && $errors->count())
    <ul class="alert danger">
        <li>Uh oh! Looks like there was an issue with your input.</li>

        @foreach ($errors->all() as $error)
            <li>
                {{ $error }}
            </li>
        @endforeach
    </ul>
@endif

@if (Session::has('success') || Session::has('status'))
    <ul class="alert success">
        <li>Success!</li>

        <li>
            {{ Session::get('success') ?? Session::get('status') ?? '' }}
        </li>
    </ul>
@endif
