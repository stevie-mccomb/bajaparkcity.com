<nav class="primary-navigation">
    <div class="wrapper">
        <a target="_blank" href="https://www.toasttab.com/baja-cantina/giftcards">
            Gift Cards
        </a>

        @foreach (\App\Models\Menu::orderBy('sort_order')->orderBy('name')->get() as $menu)
            <a href="{{ route('menus.show', [ 'slug' => $menu->slug ]) }}">
                {{ $menu->name }}
            </a>
        @endforeach

        <a href="{{ route('cantina-club') }}">
            Cantina Club
        </a>

        @if (Storage::exists('hours.json'))
            <a href="{{ route('hours') }}">
                Hours
            </a>
        @endif

        <a href="{{ route('reviews.create') }}">
            Submit a Review
        </a>

        <a href="{{ route('contact') }}">
            Contact
        </a>
    </div>
</nav>
