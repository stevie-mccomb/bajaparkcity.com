<footer class="primary-footer">
    <div class="wrapper">
        <ul class="social-links">
            <li>
                <a target="_blank" href="https://www.tiktok.com/@baja.cantina">
                    <span class="fab fa-3x fa-tiktok"></span>
                </a>
            </li>

            <li>
                <a target="_blank" href="https://www.facebook.com/BajaCantinaParkCityMexicanRestaurant/">
                    <span class="fab fa-3x fa-facebook"></span>
                </a>
            </li>
        </ul>

        <div class="copy-container">
            <p>
                Baja Cantina has been proud to serve original California-Mexican cuisine to Park City, Utah since 1983. We appreciate your continued patronage!
            </p>

            <p>
                All Content Copyright &copy; Baja Park City 1983 &ndash; {{ date('Y') }} - All Rights Reserved.
            </p>

            <ul class="auth-links">
                @auth
                    @can('view-admin')
                        <li>
                            <a href="{{ route('admin.dashboard') }}">Admin</a>
                        </li>

                        <li>|</li>
                    @endcan

                    <li>
                        <form action="{{ route('logout') }}" method="POST">
                            @csrf

                            <a href="#" onclick="this.parentNode.submit(); return false;">Log Out</a>
                        </form>
                    </li>
                @else
                    <li>
                        <a href="{{ route('login') }}">Log In</a>
                    </li>
                @endauth
            </ul>
        </div>
    </div>
</footer>
