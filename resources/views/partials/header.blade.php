<header class="announcement-banner">
    <div class="wrapper">
        <p>
            For curb-side pickup, call us at <a href="tel:4356492252" style="color: white; text-decoration: underline;">(435) 649-2252</a>
        </p>

        <p class="buttons">
            <a target="_blank" href="https://order.toasttab.com/online/baja-cantina">
                Order Pickup
            </a>

            <a target="_blank" href="https://www.mountainexpressdelivery.com/order/restaurant/baja-cantina---mexican--menu/23">
                Order Delivery
            </a>
        </p>
    </div>
</header>

<header class="primary-header">
    <div class="wrapper">
        <div class="thirds">
            <div class="third">
                <h3>
                    <a href="tel:4356492252">
                        &lpar;435&rpar; 649-2252
                    </a>
                </h3>
            </div>

            <div class="third logo-third">
                <a href="/">
                    <img src="/img/logo.png" alt="Baja Cantina - Click to visit homepage">
                </a>
            </div>

            <div class="third">
                <h3>
                    <a target="_blank" href="https://www.google.com/maps/place/Baja+Cantina/@40.6514471,-111.5101054,17z/data=!4m2!3m1!1s0x8752729c20b23f73:0x3bdf91c9b95ea113">
                        1355 Lowell Ave, Park City Mountain Resort, Park City, UT - 84060
                    </a>
                </h3>
            </div>
        </div>
    </div>
</header>
