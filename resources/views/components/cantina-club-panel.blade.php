@props([
    'class' => '',
    'icon' => 'party-horn',
])

<div class="cantina-club-panel">
    <header>
        <span class="title-icon fal fa-fw fa-{{ $icon }}"></span>

        <h3>{{ $title }}</h3>
    </header>

    <section class="{{ $class }} {{ !empty($footer) ? 'has-footer' : '' }}">
        {{ $slot }}
    </section>

    @if (!empty($footer))
        <footer>
            {{ $footer }}
        </footer>
    @endif
</div>
