@props([
    'coupon',
])

@once
    @push('styles')
        <style type="text/css">
            .coupon-component {
                padding: 0.5rem;
                background: #eee;
                border-radius: 0.25rem;
                margin-bottom: 1rem;
                box-shadow: {{ config('styles.shadow') }};
            }

            .coupon-component .inner-border {
                display: flex;
                flex-direction: column;
                justify-content: center;
                align-items: center;
                padding: 1rem;
                border: 1px dashed #999;
                border-radius: 0.25rem;
            }

            .coupon-component p {
                max-width: 72ch;
                text-align: center;
            }

            .coupon-component p:last-of-type {
                margin-bottom: 0;
            }

            .coupon-component .description {
                color: black;
                font-size: 1.5rem;
                font-weight: 800;
                margin-top: 2rem;
                margin-bottom: 1rem;
            }

            .coupon-component .code {
                padding: 0.5rem;
                border-radius: 0.25rem;
                background: #ddd;
                border: 1px solid #ccc;
                font-style: italic;
            }

            .coupon-component .terms {
                margin-top: 2rem;
                font-size: 0.8rem;
                font-style: italic;
            }
        </style>
    @endpush
@endonce

<div class="coupon-component">
    <div class="inner-border">
        <p class="description">
            {!! str_replace("\n", '<br>', $coupon->description) !!}
        </p>

        @if (!empty($coupon->code))
            <p class="code">
                {{ $coupon->code }}
            </p>
        @endif

        @if (!empty($coupon->terms))
            <p class="terms">
                {!! str_replace("\n", '<br>', $coupon->terms) !!}
            </p>
        @endif
    </div>
</div>
