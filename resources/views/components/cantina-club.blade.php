@extends('layouts.app')

@section('content')
    <div class="cantina-club-layout">
        <div class="wrapper">
            <div class="cantina-club-container">
                <header>
                    <h1>Cantina Club</h1>
                </header>

                <section>
                    @include('cantina-club.sidebar')

                    <main>
                        <header>
                            <h2>
                                {{ $title ?? 'Cantina Club' }}
                            </h2>
                        </header>

                        <section class="{{ $class ?? '' }}">
                            {{ $slot }}
                        </section>
                    </main>
                </section>
            </div>
        </div>
    </div>
@endsection
