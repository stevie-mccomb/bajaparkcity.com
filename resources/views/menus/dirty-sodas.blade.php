@extends('layouts.app')

@push('styles')
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Yellowtail&display=swap" rel="stylesheet">

    <style type="text/css">
        .dirty-soda-view {
            background: #ddd;
            padding: 2rem 0;
        }

        @media (max-width: 639px) {
            .dirty-soda-view {
                padding: 0;
            }
        }
    </style>
@endpush

@section('content')
    <div class="dirty-soda-view">
        <dirty-soda-menu :menu="{{ $menu }}"></dirty-soda-menu>
    </div>
@endsection
