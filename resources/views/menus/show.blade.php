@extends('layouts.app')

@section('content')
    <div class="padded wrapper">
        <div class="menu">
            <h1 class="menu-name">
                {{ $menu->name }}
            </h1>

            @if (!empty($menu->content))
                @if (count($menu->content->sections))
                    @foreach ($menu->content->sections as $key => $section)
                        <div class="menu-section">
                            <h2 class="section-name">
                                {{ $section->name }}
                            </h2>

                            @if (count($section->items))
                                @foreach ($section->items as $item)
                                    <div class="menu-item">
                                        <h3 class="name">
                                            {{ $item->name }}

                                            {{-- @if (!empty($item->flags))
                                                @foreach ($item->flags as $flag)
                                                    <span class="flag">{{ $flag }}</span>
                                                @endforeach
                                            @endif --}}
                                        </h3>

                                        @if (!empty($item->description))
                                            <span class="description">
                                                &nbsp;- {{ $item->description }}
                                            </span>
                                        @endif

                                        @if (!empty($item->price))
                                            <span class="price">
                                                {{ $item->price }}
                                            </span>
                                        @endif
                                    </div>
                                @endforeach
                            @endif
                        </div>
                    @endforeach
                @endif
            @endif

            <p>
                <strong>
                    * CAUTION Consuming raw or undercooked meats or seafood may increase your risk of foodborne illness.
                </strong>
            </p>
        </div>
    </div>
@endsection
