@include('partials.start')
@include('partials.header')
@include('partials.nav')

<div class="admin-container padded wrapper">
    @include ('admin.sidebar')

    <div class="admin-content">
        @yield('content')
    </div>
</div>

@include ('partials.footer')
@include ('partials.end')
