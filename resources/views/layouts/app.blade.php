@include('partials/start')
@include('partials/header')
@include('partials/nav')

@yield('content')

@include('partials/footer')
@include('partials/end')
