@extends('layouts.app')

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>
                Contact Us
            </h1>
        </div>
    </div>

    <div class="padded wrapper">
        <recaptcha action="contact" g-recaptcha-key="{{ config('services.google.recaptcha.key') }}">
            <form class="contact-form" action="{{ route('contact.send') }}" method="POST">
                @csrf

                @include('partials.alerts')

                <div class="form-row">
                    <div class="form-group required">
                        <label for="first_name">First Name</label>

                        <input id="first_name" name="first_name" type="text" value="{{ old('first_name') }}">
                    </div>

                    <div class="form-group required">
                        <label for="last_name">Last Name</label>

                        <input id="last_name" name="last_name" type="text" value="{{ old('last_name') }}">
                    </div>
                </div>

                <div class="form-row">
                    <div class="form-group required">
                        <label for="email">Email</label>

                        <input id="email" name="email" type="email" value="{{ old('email') }}">
                    </div>

                    <div class="form-group">
                        <label for="phone">Phone</label>

                        <input id="phone" name="phone" type="text" value="{{ old('phone') }}">
                    </div>
                </div>

                <div class="form-group required">
                    <label for="message">Message</label>

                    <textarea id="message" name="message" rows="5">{{ old('message') }}</textarea>
                </div>

                <button class="button" type="submit" >
                    <span class="fal fa-envelope"></span> Send Message
                </button>
            </recaptcha>
        </form>
    </div>
@endsection
