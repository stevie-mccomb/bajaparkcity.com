@extends('layouts.app')

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>
                Thank you!
            </h1>
        </div>
    </div>

    <div class="contact-thank-you padded wrapper">
        <p>Thank you! We have received your message and will be in touch shortly.</p>
    </div>
@endsection
