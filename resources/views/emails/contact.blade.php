<x-mail::message>
# Website Contact Form

<x-mail::table>
| Field      | Value                              |
|:-----------|:-----------------------------------|
| First Name | {{ $request->safe()->first_name }} |
| Last Name  | {{ $request->safe()->last_name }}  |
| Email      | {{ $request->safe()->email }}      |
@if (!empty($request->safe()->phone))
| Phone      | {{ $request->safe()->phone }}      |
@endif
</x-mail::table>

{{ $request->safe()->message }}
</x-mail::message>
