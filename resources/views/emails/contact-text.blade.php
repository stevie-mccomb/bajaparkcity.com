Website Contact Form

First Name: {{ $request->safe()->first_name }}
Last Name: {{ $request->safe()->last_name }}
Email: {{ $request->safe()->email }}
@if (!empty($request->safe()->phone))
Phone: {{ $request->safe()->phone }}
@endif

{{ $request->safe()->message }}
