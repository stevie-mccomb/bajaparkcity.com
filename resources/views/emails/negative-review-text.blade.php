Negative Review

First Name: {{ $request->safe()->first_name }}
Last Name: {{ $request->safe()->last_name }}
Email: {{ $request->safe()->email }}
@if (!empty($request->safe()->phone))
Phone: {{ $request->safe()->phone }}
@endif

When was the Visit? {{ $request->safe()->when }}
Serving Staff: {{ $request->safe()->server ?? 'Unknown' }}
Spoke to Manager? {{ ucfirst($request->safe()->spoke_to_manager) }}
Manager Name(s): {{ $request->safe()->manager ?? 'Unknown' }}
Were any other staff members involved? {{ ucfirst($request->safe()->other_staff_involved) }}
Other Staff Members: {{ $request->safe()->other_staff ?? 'Unknown' }}

{{ $request->safe()->details }}
