@extends('layouts.app')

@section('content')
    <div class="hero" style="background-image: url('/img/hero.jpg');">
        <div class="wrapper">
            <div class="overlay" style="position: relative; text-align: center;">
                <h2 style="margin-bottom: 1rem;">Come Try our new Gluten Free menu</h2>

                <p>All of our sauces are now 100% gluten free.</p>
            </div>
        </div>
    </div>

    <div class="index-content">
        <div class="wrapper">
            <div class="news-and-events">
                <h1>News and Events</h1>

                <img src="/img/pepper.png" alt="Divider">

                <p>Have a birthday to celebrate or a company event to plan?</p>

                <p>Leave the fiesta to us, amigo!</p>

                <p>Call <a href="4356492252">(435) 649-BAJA</a> or email us at <a href="mailto:hello@bajaparkcity.com">hello@bajaparkcity.com</a> and let us help you celebrate!</p>
            </div>

            <div class="home-grid">
                <div class="grid-cell">
                    <div class="cell-content">
                        <a target="_blank" href="https://www.toasttab.com/baja-cantina/giftcards">
                            <img src="/img/gift-card-button.jpg" alt="Buy a Gift Card Today!">
                        </a>
                    </div>
                </div>

                <div class="grid-cell">
                    <div class="cell-content">
                        <img src="/img/fajitas.png" alt="Beef Fajitas">
                    </div>
                </div>

                <div class="grid-cell">
                    <div class="cell-content">
                        <img src="/img/flautas.png" alt="Chicken Flautas">
                    </div>
                </div>

                <div class="grid-cell">
                    <div class="cell-content">
                        <img src="/img/quesadilla.png" alt="Quesadilla">
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
