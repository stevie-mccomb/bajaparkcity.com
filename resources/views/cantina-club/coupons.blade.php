@push('styles')
    <style type="text/css">
        .global-coupon-disclaimer {
            max-width: 72ch;
            text-align: center;
            font-size: 0.8rem;
            font-style: italic;
            margin: 2rem auto 0 auto;
        }
    </style>
@endpush

<x-cantina-club class="dashboard">
    <x-slot:title>
        Coupons de Cocina
    </x-slot>

    <x-cantina-club-panel icon="badge-dollar">
        <x-slot:title>
            Active Coupons de Cocina
        </x-slot:header>

        @if ($eligibleCoupons->count())
            @foreach ($eligibleCoupons as $coupon)
                <x-coupon :coupon="$coupon" />
            @endforeach

            <p class="global-coupon-disclaimer">
                Coupons are limited to one per order; coupons cannot be used in combination with each other. A single order cannot use coupons and Pico Points in combination. Orders placed with coupons will still earn you Pico Points, though the value of the points will be based on the discounted total, not the pre-discount total.
            </p>
        @else
            No active coupons, check back soon!
        @endif
    </x-cantina-club-panel>

    <x-cantina-club-panel icon="clock-rotate-left">
        <x-slot:title>
            Redeemed Coupons
        </x-slot:header>

        @if ($redeemedCoupons->count())
            @foreach ($redeemedCoupons as $coupon)
                <x-coupon :coupon="$coupon" />
            @endforeach
        @else
            No redeemed coupons.

            @if ($eligibleCoupons->count())
                Redeem one in the restaurant today!
            @endif
        @endif
    </x-cantina-club-panel>
</x-cantina-club>
