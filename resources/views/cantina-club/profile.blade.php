<x-cantina-club>
    <x-slot:title>
        My Profile
    </x-slot>

    @include('partials.alerts')

    <form method="POST">
        @csrf @method('PATCH')

        <div class="form-row">
            <div class="form-group required">
                <label for="first_name">First Name</label>

                <input id="first_name" name="first_name" type="text" value="{{ old('first_name', auth()->user()->first_name) }}" required autofocus max="255" autocomplete="first-name">
            </div>

            <div class="form-group required">
                <label for="last_name">Last Name</label>

                <input id="last_name" name="last_name" type="text" value="{{ old('last_name', auth()->user()->last_name) }}" required max="255" autocomplete="last-name">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group required">
                <label for="email">Email</label>

                <input id="email" name="email" type="email" value="{{ old('email', auth()->user()->email) }}" required max="255" autocomplete="email">
            </div>

            <div class="form-group">
                <label for="phone">Phone</label>

                <input id="phone" name="phone" type="tel" value="{{ old('phone', auth()->user()->phone) }}" max="32" autocomplete="phone">
            </div>
        </div>

        <div class="form-row">
            <div class="form-group">
                <label for="new_password">New Password</label>

                <input id="new_password" name="new_password" type="password" value="{{ old('password') }}" autocomplete="new-password">
            </div>

            <div class="form-group">
                <label for="new_password_confirmation">Confirm New Password</label>

                <input id="new_password_confirmation" name="new_password_confirmation" type="password" value="{{ old('new_password_confirmation') }}" autocomplete="new-password">
            </div>
        </div>

        <button class="button" type="submit">
            <span class="fal fa-check-circle"></span> Save Profile
        </button>
    </form>
</x-cantina-club>
