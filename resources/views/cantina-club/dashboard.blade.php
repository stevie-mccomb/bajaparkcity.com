@push('styles')
    <style type="text/css">
        .panels {
            display: grid;
            grid-template-columns: 1fr 2fr;
            gap: 1rem;
        }

        .pico-points {
            display: flex;
            align-items: center;
            gap: 1rem;
            font-size: 1rem;
        }

        .pico-points .point-value {
            display: flex;
            align-items: center;
        }

        .pico-points .point-value-number {
            font-size: 2rem;
            color: black;
            font-weight: 800;
            margin-right: 0.5rem;
        }

        .global-coupon-disclaimer {
            max-width: 72ch;
            text-align: center;
            font-size: 0.8rem;
            font-style: italic;
            margin: 2rem auto 0 auto;
        }

        @media (max-width: 959px) {
            .dashboard {
                grid-template-columns: 1fr 1fr;
            }
        }

        @media (max-width: 639px) {
            .dashboard {
                grid-template-columns: 1fr;
            }
        }
    </style>
@endpush

<x-cantina-club class="dashboard">
    <x-slot:title>
        Dashboard
    </x-slot>

    @include('partials.alerts')

    <div class="panels">
        <x-cantina-club-panel icon="taco" class="pico-points">
            <x-slot:title>
                Pico Points <tooltip v-cloak>Pico Points can be spent at the restaurant just like cash! Earn Pico Points with every dollar you spend at the restaurant, just provide your Cantina Club membership email with each purchase to link those points to your Cantina Club account.</tooltip>
            </x-slot:title>

            <span class="point-value">
                <span class="point-value-number">{{ auth()->user()->points }}</span> Pico Points
            </span>

            <x-slot:footer>
                <a href="{{ route('cantina-club.points') }}">
                    View Details

                    <span class="fal fa-angle-right"></span>
                </a>
            </x-slot>
        </x-cantina-club-panel>

        <x-cantina-club-panel icon="badge-dollar" class="active-deals">
            <x-slot:title>
                Coupons de Cocina
            </x-slot:title>

            @if ($coupons->count())
                @foreach ($coupons as $coupon)
                    <x-coupon :coupon="$coupon" />
                @endforeach

                <p class="global-coupon-disclaimer">
                    Coupons are limited to one per order; coupons cannot be used in combination with each other. A single order cannot use coupons and Pico Points in combination. Orders placed with coupons will still earn you Pico Points, though the value of the points will be based on the discounted total, not the pre-discount total.
                </p>
            @else
                No active coupons, check back soon!
            @endif
        </x-cantina-club-panel>
    </xpanels>
</x-cantina-club>
