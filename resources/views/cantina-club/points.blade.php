@push('styles')
    <style type="text/css">
        .pico-points {
            display: flex;
            justify-content: space-between;
            align-items: center;
            gap: 1rem;
            font-size: 1rem;
        }

        .pico-points .point-value, .pico-points .dollar-value {
            display: flex;
            align-items: center;
        }

        .pico-points .point-value-number {
            font-size: 2rem;
            color: black;
            font-weight: 700;
            margin-right: 1rem;
        }

        .pico-points .dollar-value-number {
            font-weight: 700;
            margin-right: 0.25rem;
        }

        .verified-actions {
            padding: 2rem !important;
            display: grid;
            grid-template-columns: 1fr 1fr;
            gap: 2rem;
        }

        .verified-action {
            display: flex;
            flex-direction: column;
            background: #eee;
            padding: 1rem;
            border-radius: 0.25rem;
            border: 1px solid #ccc;
            box-shadow: {{ config('styles.shadow') }};
        }

        .verified-action header {
            display: flex;
            justify-content: space-between;
            align-items: flex-start;
        }

        .verified-action h3 {
            line-height: 1.5;
            align-self: flex-end;
            margin-bottom: 1rem;
            font-size: 1.25rem;
        }

        .verified-action .auto-badge {
            padding: 0.25rem 1rem;
            border-radius: 10rem;
            background: #ddd;
            display: flex;
            justify-content: center;
            align-items: center;
            border: 1px solid #ccc;
        }

        .verified-action p {
            margin-bottom: 2rem;
        }

        .verified-action footer {
            display: flex;
            justify-content: space-between;
            align-items: center;
            gap: 2rem;
            margin-top: auto;
        }

        .verified-action .verified {
            flex: 1;
            display: flex;
            justify-content: space-between;
            border-radius: 0.25rem;
            padding: 1rem;
            color: black;
            font-weight: 700;
            background: linear-gradient(135deg, limegreen, yellowgreen);
        }

        .verified-action .button {
            flex: 1;
            display: flex;
            justify-content: space-between;
        }

        .verified-action .point-value {
            color: black;
            font-weight: 700;
        }

        .historical-point-adjustment {
            display: flex;
            align-items: center;
            background: #eee;
            border-radius: 1rem;
            margin-top: 1rem;
            border: 1px solid #ccc;
        }

        .historical-point-adjustment:first-of-type {
            margin-top: 0;
        }

        .historical-point-adjustment .value {
            padding: 1rem;
            color: black;
            font-size: 1.25rem;
            font-weight: 700;
            white-space: nowrap;
        }

        .historical-point-adjustment .reason {
            padding: 1rem;
            flex: 1;
            line-height: 1.5;
            border-right: 1px solid #ccc;
            border-left: 1px solid #ccc;
        }

        .historical-point-adjustment .when {
            padding: 1rem;
            white-space: nowrap;
        }

        .form-help {
            max-width: 72ch;
            margin: 0 auto;
            text-align: center;
        }

        @media (max-width: 959px) {
            .verified-actions {
                grid-template-columns: 1fr;
            }

            .verified-action header {
                flex-direction: column-reverse;
                align-items: center;
                gap: 1rem;
            }

            .verified-action h3 {
                align-self: center;
            }

            .verified-action p {
                text-align: center;
            }

            .verified-action footer {
                gap: 1rem;
            }
        }

        @media (max-width: 719px) {
            .verified-action footer {
                flex-direction: column-reverse;
                align-items: initial;
            }

            .verified-action .point-value {
                align-self: center;
            }
        }

        @media (max-width: 639px) {
            .verified-actions {
                grid-template-columns: 1fr 1fr;
            }

            .verified-action header {
                align-items: flex-start;
            }

            .verified-action h3 {
                align-self: inherit;
            }

            .verified-action p {
                text-align: left;
            }

            .verified-action .point-value {
                align-self: inherit;
            }
        }

        @media (max-width: 479px) {
            .verified-actions {
                grid-template-columns: 1fr;
            }
        }
    </style>
@endpush

<x-cantina-club class="dashboard">
    <x-slot:title>
        Pico Points
    </x-slot>

    <x-cantina-club-panel icon="taco" class="pico-points">
        <x-slot:title>
            Available Pico Points

            <tooltip v-cloak>
                Pico Points can be spent at the restaurant just like cash! Earn Pico Points with every dollar you spend at the restaurant, just provide your Cantina Club membership email with each purchase to link those points to your Cantina Club account.
            </tooltip>
        </x-slot:header>

        <span class="point-value">
            <span class="point-value-number">{{ auth()->user()->points }}</span> Pico Points
        </span>

        <span class="dollar-value">
            &lpar;<span class="dollar-value-number">{{ auth()->user()->pointsToDollars }}</span> Dollar Value&rpar;
        </span>

        <x-slot:footer>
            <spend-points :user="{{ auth()->user()->append([ 'points' ]) }}" url="{{ route('admin.point-adjustments.create-from-qr-code', auth()->user()) }}">
                Spend Points <span class="fal fa-angle-right"></span>
            </spend-points>
        </x-slot>
    </x-cantina-club-panel>

    @if ($verifiedActions->count())
        <x-cantina-club-panel icon="location-plus" class="verified-actions">
            <x-slot:title>
                Earn Points

                <tooltip v-cloak>
                    Complete the following actions to earn points. Some actions will automatically award the points (after a short delay), others require manual verification. For manual actions, talk to a manager in the restaurant to earn your points. If an automatic action doesn't automatically award you points, let a manager know and they can correct it.
                </tooltip>
            </x-slot:title>

            <div class="verified-action">
                <header>
                    <h3>Make a Purchase</h3>

                    <span class="auto-badge">
                        Manual
                    </span>
                </header>

                <p>Come into the restaurant and make a purchase. Let your server or manager know your Cantina Club email address to earn your points!</p>

                <footer>
                    <span>
                        (Limited to one purchase per day)
                    </span>

                    <span class="point-value">
                        +10 points
                    </span>
                </footer>
            </div>

            @foreach ($verifiedActions as $verifiedAction)
                <div class="verified-action">
                    <header>
                        <h3>{{ $verifiedAction->name }}</h3>

                        <span class="auto-badge">
                            {{ $verifiedAction->automatic ? 'Auto' : 'Manual' }}
                        </span>
                    </header>

                    <p>{{ $verifiedAction->description }}</p>

                    <footer>
                        @if ($verifiedAction->verified)
                            <span class="verified">
                                <span>Completed</span>

                                <span class="fal fa-check-circle"></span>
                            </span>
                        @elseif ($verifiedAction->url)
                            <a href="{{ route('cantina-club.verified-actions.visit', $verifiedAction) }}" class="button" target="_blank">
                                <span>{{ $verifiedAction->button }}</span>

                                <span class="fal fa-long-arrow-right"></span>
                            </a>
                        @else
                            <span></span>
                        @endif

                        <span class="point-value">
                            +{{ $verifiedAction->value }} points
                        </span>
                    </footer>
                </div>
            @endforeach
        </x-cantina-club-panel>
    @endif

    <x-cantina-club-panel icon="clock-rotate-left" class="point-adjustments">
        <x-slot:title>
            Point History

            <tooltip v-cloak>
                This list shows each adjustment that has been made to your point value over time.
            </tooltip>
        </x-slot:header>

        @if ($pointAdjustments->count())
            @foreach ($pointAdjustments as $pointAdjustment)
                <div class="historical-point-adjustment">
                    <span class="value">
                        {{ $pointAdjustment->value }}
                    </span>

                    <span class="reason">
                        {{ $pointAdjustment->reason }}
                    </span>

                    <span class="when">
                        {{ $pointAdjustment->created_at->format('F jS, Y') }}
                    </span>
                </div>
            @endforeach

            {{ $pointAdjustments->appends(request()->all())->links() }}
        @else
            <p>No point history found.</p>
        @endif
    </x-cantina-club-panel>

    <p class="form-help">
        If you feel that there is an error in your point history or available point balance, please inform management so it can be reviewed and corrected.
    </p>
</x-cantina-club>
