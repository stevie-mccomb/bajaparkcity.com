<aside class="cantina-club-sidebar">
    <a href="{{ route('cantina-club') }}" class="{{ ActiveRoute::is('cantina-club') }}">
        <span class="icon-container">
            <span class="fal fa-lg fa-fw fa-grid-2"></span>
        </span>

        <span class="name">
            Dashboard
        </span>
    </a>

    <a href="{{ route('cantina-club.points') }}" class="{{ ActiveRoute::is('cantina-club.points') }}">
        <span class="icon-container">
            <span class="fal fa-lg fa-fw fa-taco"></span>
        </span>

        <span class="name">
            Pico Points
        </span>
    </a>

    <a href="{{ route('cantina-club.coupons') }}" class="{{ ActiveRoute::is('cantina-club.coupons') }}">
        <span class="icon-container">
            <span class="fal fa-lg fa-fw fa-badge-dollar"></span>
        </span>

        <span class="name">
            Coupons de Cocina
        </span>
    </a>

    <a href="{{ route('cantina-club.profile') }}" class="{{ ActiveRoute::is('cantina-club.profile') }}">
        <span class="icon-container">
            <span class="fal fa-lg fa-fw fa-user"></span>
        </span>

        <span class="name">
            My Profile
        </span>
    </a>

    <form action="{{ route('logout') }}" method="POST">
        @csrf

        <a href="#" onclick="this.parentNode.submit();">
            <span class="icon-container">
                <span class="fal fa-lg fa-fw fa-lock"></span>
            </span>

            <span class="name">
                Log Out
            </span>
        </a>
    </form>
</aside>
