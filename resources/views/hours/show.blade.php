@extends('layouts.app')

@push('styles')
    <style type="text/css">
        .hours-index :is(strong, b) {
            font-weight: bold;
        }

        .hours-index :is(em, i) {
            font-style: italic;
        }

        .hours-index a {
            color: #085;
            text-decoration: underline;
        }
    </style>
@endpush

@section('content')
    <div class="page-header">
        <div class="wrapper">
            <h1>
                Hours
            </h1>
        </div>
    </div>

    <div class="hours-index padded wrapper">
        @foreach ($hours->blocks as $block)
            @switch ($block->type)
                @case('paragraph')
                    <p>
                        {!! $block->data->text !!}
                    </p>
                    @break
            @endswitch
        @endforeach
    </div>
@endsection
