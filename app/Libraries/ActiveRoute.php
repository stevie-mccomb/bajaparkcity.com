<?php

namespace App\Libraries;

use Request;
use Route;

class ActiveRoute
{
    /**
     * Format and prepare the given path as a relative route.
     *
     * @param  string  $path
     * @param  array   $args
     * @return string
     */
    private static function filter($path, $args = array())
    {
        if (Route::has($path)) $path = empty($args) ? route($path) : route($path, $args);

        $url = config('app.url');
        $isAbsolute = substr($path, 0, strlen($url)) === $url;

        if ($isAbsolute) $path = substr($path, strlen($url));

        if (substr($path, 0, 1) === '/') $path = substr($path, 1);

        if (empty($path)) $path = '/';

        return $path;
    }

    /**
     * Returns the given CSS class (default 'active')
     * if the current route matches any of the given paths.
     *
     * @param  string|array  $paths
     * @param  string        $class
     * @return string
     */
    public static function is($paths, $class = 'active')
    {
        if (gettype($paths) === 'string') $paths = [$paths];

        if (is_array($paths)) {
            foreach ($paths as $key => $value) {
                $path = is_array($value) ? self::filter($key, $value) : self::filter($value);

                if (Request::is($path)) return $class;
            }
        }

        return '';
    }

    /**
     * Returns the given CSS class (default 'active')
     * if the current route starts with any of the given paths.
     *
     * @param  string|array  $paths
     * @param  string        $class
     * @return string
     */
    public static function startsWith($paths, $class = 'active')
    {
        if (gettype($paths) === 'string') $paths = [$paths];

        if (is_array($paths)) {
            foreach ($paths as $key => $value) {
                $path = is_array($value) ? self::filter($key, $value) : self::filter($value);

                if (Request::is($path) || Request::is("$path/*")) return $class;
            }
        }

        return '';
    }

    /**
     * Returns the given CSS class (default 'active')
     * if the current route ends with any of the given paths.
     *
     * @param  string|array  $paths
     * @param  string        $class
     * @return string
     */
    public static function endsWith($paths, $class = 'active')
    {
        if (gettype($paths) === 'string') $paths = [$paths];

        if (is_array($paths)) {
            foreach ($paths as $key => $value) {
                $path = is_array($value) ? self::filter($key, $value) : self::filter($value);

                if (Request::is($path) || Request::is("*/$path")) return $class;
            }
        }

        return '';
    }

    /**
     * Returns the given CSS class (default 'active')
     * if the current route does not match any of the given routes.
     *
     * @param  string|array  $paths
     * @param  string        $class
     * @return string
     */
    public static function not($paths, $class = 'active')
    {
        if (gettype($paths) === 'string') $paths = [$paths];

        if (is_array($paths)) {
            foreach ($paths as $key => $value) {
                $path = is_array($value) ? self::filter($key, $value) : self::filter($value);

                if (Request::is($path)) return '';
            }
        }

        return $class;
    }
}
