<?php

use App\Models\AuditLogEvent;
use Illuminate\Database\Eloquent\Model;

function audit_log(Model $resource, string $event, string $summary, mixed $data = null): AuditLogEvent
{
    if (empty($event)) throw new Exception('$event is required when creating an audit log event.');
    if (empty($summary)) throw new Exception('$summary is required when creating an audit log event.');

    $fields = [
        'auditable_type' => $resource::class,
        'auditable_id' => $resource->id,
        'user_id' => auth()->user()->id ?? null,
        'event' => $event,
        'summary' => $summary,
    ];

    if (!empty($data)) {
        $fields['data'] = is_json($data) ? $data : json_encode($data);
    }

    return AuditLogEvent::create($fields);
}

function is_json(mixed $value): bool
{
    try {
        json_decode($value);
        return json_last_error() === JSON_ERROR_NONE;
    } catch (Throwable $e) {
        return false;
    }
}

function pointsToDollars(int $points): string
{
    $value = max(
        round(
            num: pointsToPennies($points) / 100,
            precision: 2,
            mode: PHP_ROUND_HALF_DOWN,
        ),
        0.0
    );

    return (new NumberFormatter('en_US', NumberFormatter::CURRENCY))->formatCurrency($value, 'USD');
}

function pointsToPennies(int $points): int
{
    return $points * 5;
}
