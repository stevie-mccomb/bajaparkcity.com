<?php

namespace App\Providers;

use App\Models\User;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::before(function(User $user, string $capability) {
            if ($user->hasRole('admin')) return true;

            $capabilities = [
                'manager' => [
                    'manage-hours',
                    'manage-menus',
                    'manage-points',
                    'manage-coupons',
                    'view-any-verified-action',
                    'verify-actions',
                    'view-admin',
                    'view-users',
                ],

                'guest' => []
            ];

            $role = $user->role->slug ?? 'guest';

            return !empty($capabilities[$role]) ? in_array($capability, $capabilities[$role]) : false;
        });
    }
}
