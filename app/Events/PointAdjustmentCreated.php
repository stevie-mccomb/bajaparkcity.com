<?php

namespace App\Events;

use App\Models\PointAdjustment;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class PointAdjustmentCreated
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * The point adjustment that was created.
     */
    public PointAdjustment $pointAdjustment;

    /**
     * Create a new event instance.
     */
    public function __construct(PointAdjustment $pointAdjustment)
    {
        $this->pointAdjustment = $pointAdjustment;
    }
}
