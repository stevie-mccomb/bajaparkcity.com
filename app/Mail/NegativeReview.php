<?php

namespace App\Mail;

use App\Http\Requests\NegativeReviewRequest;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;

class NegativeReview extends Mailable
{
    /**
     * The HTTP request containing the user-provided input.
     */
    public NegativeReviewRequest $request;

    /**
     * Create a new message instance.
     */
    public function __construct(NegativeReviewRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            to: config('mail.from.address'),
            replyTo: $this->request->safe()->email,
            subject: 'Negative Review',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.negative-review',
            text: 'emails.negative-review-text',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
