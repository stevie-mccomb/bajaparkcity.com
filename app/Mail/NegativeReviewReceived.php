<?php

namespace App\Mail;

use App\Http\Requests\NegativeReviewRequest;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Mail\Mailables\Content;
use Illuminate\Mail\Mailables\Envelope;
use Illuminate\Queue\SerializesModels;

class NegativeReviewReceived extends Mailable
{
    /**
     * The HTTP request with the user-provided input.
     */
    public NegativeReviewRequest $request;

    /**
     * Create a new message instance.
     */
    public function __construct(NegativeReviewRequest $request)
    {
        $this->request = $request;
    }

    /**
     * Get the message envelope.
     */
    public function envelope(): Envelope
    {
        return new Envelope(
            to: $this->request->safe()->email,
            replyTo: config('mail.from.address'),
            subject: 'Review Received',
        );
    }

    /**
     * Get the message content definition.
     */
    public function content(): Content
    {
        return new Content(
            markdown: 'emails.negative-review-received',
            text: 'emails.negative-review-received-text',
        );
    }

    /**
     * Get the attachments for the message.
     *
     * @return array<int, \Illuminate\Mail\Mailables\Attachment>
     */
    public function attachments(): array
    {
        return [];
    }
}
