<?php

namespace App\Listeners;

use App\Events\PointAdjustmentCreated;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Cache;

class ClearUserPointCache
{
    /**
     * Create the event listener.
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     */
    public function handle(PointAdjustmentCreated $event): void
    {
        $userId = $event->pointAdjustment->user_id;
        Cache::forget("user:{$userId}.points");
    }
}
