<?php

namespace App\Http\Controllers;

use App\Models\Menu;
use Illuminate\Http\Request;
use Illuminate\View\View;

class MenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $limit = $request->has('limit') ? max((Int) $request->input('limit'), 20) : 20;
        $offset = $request->has('offset') ? min((Int) $request->input('offset'), 0) : 0;

        $query = Menu::skip($offset)->take($limit);

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['menus'] = $query->get();

        return view('admin.menus.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['menu'] = new Menu;

        return view('admin.menus.edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:32',
            'slug' => 'required|string|max:32',
            'content' => 'required|json',
            'sort_order' => 'integer',
        ]);

        $menu = Menu::create($request->only(['name', 'slug', 'content', 'sort_order']));

        audit_log(
            resource: $menu,
            event: 'Menu Created',
            summary: "{$request->user()->name} created new \"{$menu->name}\" menu.",
            data: $menu->toJson(),
        );

        return redirect("admin/menus/{$menu->id}/edit")->with('success', 'Menu successfully created.');
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $data['menu'] = Menu::where('menus.slug', $slug)->firstOrFail();

        return view('menus.show', $data);
    }

    /**
     * Display the specified resource.
     */
    public function dirtySodas(): View
    {
        $data['menu'] = Menu::where('menus.slug', 'dirty-sodas')->firstOrFail();

        return view('menus.dirty-sodas', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function edit(Menu $menu)
    {
        $data['menu'] = $menu;

        return view('admin.menus.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Menu          $menu
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Menu $menu)
    {
        $this->validate($request, [
            'name' => 'required|string|max:32',
            'content' => 'required|json',
            'sort_order' => 'integer',
        ]);

        $menu->update($request->only(['name', 'content', 'sort_order']));

        $changes = $menu->getChanges();

        if (!empty($changes)) {
            audit_log(
                resource: $menu,
                event: 'Menu Updated',
                summary: "{$request->user()->name} updated \"{$menu->name}\" menu.",
                data: $changes,
            );
        }

        return redirect("admin/menus/{$menu->id}/edit")->with('success', 'Menu successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Menu  $menu
     * @return \Illuminate\Http\Response
     */
    public function destroy(Menu $menu)
    {
        $menu->delete();

        audit_log(
            resource: $menu,
            event: 'Menu Deleted',
            summary: auth()->user()->name . " deleted \"{$menu->name}\" menu.",
        );

        return redirect(route('admin.menus.index'))->with('success', 'Menu successfully deleted.');
    }
}
