<?php

namespace App\Http\Controllers;

use App\Models\VerifiedAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class PointController extends Controller
{
    /**
     * Display the points screen.
     */
    public function index(): View
    {
        $data['pointAdjustments'] = auth()->user()->pointAdjustments()->orderBy('created_at', 'desc')->paginate(25);
        $data['verifiedActions'] = VerifiedAction::orderBy('value', 'desc')->get();

        return view('cantina-club.points', $data);
    }

    /**
     * Verify the given action for the current user and redirect them to the action's URL.
     */
    public function visitVerifiedAction(VerifiedAction $verifiedAction): RedirectResponse
    {
        if ($verifiedAction->automatic) {
            $verifiedAction->verify(auth()->user());
        }

        return redirect($verifiedAction->url);
    }
}
