<?php

namespace App\Http\Controllers;

use App\Http\Requests\Coupons\CreateRequest;
use App\Http\Requests\Coupons\DeleteRequest;
use App\Http\Requests\Coupons\StoreRequest;
use App\Http\Requests\Coupons\UpdateRequest;
use App\Models\Coupon;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(): View
    {
        $data['eligibleCoupons'] = Coupon::eligible()->get();
        $data['redeemedCoupons'] = Coupon::redeemed()->get();

        return view('cantina-club.coupons', $data);
    }
}
