<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProfileRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\View\View;

class ProfileController extends Controller
{
    /**
     * Display the form for editing the current user's profile.
     */
    public function edit(): View
    {
        return view('cantina-club.profile');
    }

    /**
     * Update the current user's profile details.
     */
    public function update(ProfileRequest $request): RedirectResponse
    {
        $data = $request->safe()->toArray();

        if (!empty($data['new_password'])) {
            $data['password'] = bcrypt($data['new_password']);
            unset($data['new_password']);
        }

        $request->user()->update($data);

        $changes = $request->user()->getChanges();
        if (!empty($changes['email'])) {
            $request->user()->email_verified_at = null;
            $request->user()->save();
            $request->user()->sendEmailVerificationNotification();
        }

        return redirect(route('cantina-club.profile'))->with('success', 'Profile successfully updated.');
    }
}
