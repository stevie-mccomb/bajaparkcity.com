<?php

namespace App\Http\Controllers;

use App\Mail\NegativeReview;
use App\Mail\NegativeReviewReceived;
use App\Http\Requests\NegativeReviewRequest;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class ReviewController extends Controller
{
    /**
     * Submit a negative review form, notifying management.
     */
    public function submitNegativeReview(NegativeReviewRequest $request): RedirectResponse
    {
        Mail::send(new NegativeReview($request));
        Mail::send(new NegativeReviewReceived($request));

        return redirect(route('reviews.thank-you'));
    }
}
