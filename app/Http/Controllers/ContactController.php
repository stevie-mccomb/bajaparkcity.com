<?php

namespace App\Http\Controllers;

use App\Http\Requests\ContactRequest;
use App\Mail\ContactMessage;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Send the given contact request via email.
     */
    public function send(ContactRequest $request): RedirectResponse
    {
        Mail::send(new ContactMessage($request));

        return redirect(route('contact.thank-you'));
    }
}
