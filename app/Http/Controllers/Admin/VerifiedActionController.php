<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\VerifiedActions\DeleteRequest;
use App\Http\Requests\VerifiedActions\IndexRequest;
use App\Http\Requests\VerifiedActions\StoreRequest;
use App\Http\Requests\VerifiedActions\UpdateRequest;
use App\Http\Requests\VerifiedActions\VerifyRequest;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\VerifiedAction;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class VerifiedActionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(IndexRequest $request): View
    {
        $query = VerifiedAction::withCount('users')->orderBy('name');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['verifiedActions'] = $query->paginate(25);

        return view('admin.verified-actions.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $data['verifiedAction'] = new VerifiedAction;

        return view('admin.verified-actions.edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $verifiedAction = VerifiedAction::create($request->safe()->toArray());

        audit_log(
            resource: $verifiedAction,
            event: 'Verified Action Created',
            summary: "{$request->user()->name} created the \"{$verifiedAction->name}\" Verified Action.",
        );

        return redirect(route('admin.verified-actions.edit', $verifiedAction))->with('success', 'Verified Action successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(VerifiedAction $verifiedAction): View
    {
        $data['verifiedAction'] = $verifiedAction;

        return view('admin.verified-actions.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, VerifiedAction $verifiedAction): RedirectResponse
    {
        $verifiedAction->update($request->safe()->toArray());

        $changes = $verifiedAction->getChanges();
        if (!empty($changes)) {
            audit_log(
                resource: $verifiedAction,
                event: 'Verified Action Updated',
                summary: "{$request->user()->name} updated the \"{$verifiedAction->name}\" Verified Action.",
                data: $changes,
            );
        }

        return redirect(route('admin.verified-actions.edit', $verifiedAction))->with('success', 'Verified Action successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DeleteRequest $request, VerifiedAction $verifiedAction): RedirectResponse
    {
        $verifiedAction->delete();

        audit_log(
            resource: $verifiedAction,
            event: 'Verified Action Archived',
            summary: "{$request->user()->name} archived the \"{$verifiedAction->name}\" Verified Action.",
        );

        return redirect(route('admin.verified-actions.index'))->with('success', 'Verified Action successfully archived.');
    }

    /**
     * Display the verify form.
     */
    public function getVerify(VerifiedAction $verifiedAction): View
    {
        $data['verifiedAction'] = $verifiedAction;

        return view('admin.verified-actions.verify', $data);
    }

    /**
     * Verify that the given user(s) have completed the given action.
     */
    public function postVerify(VerifyRequest $request, VerifiedAction $verifiedAction): RedirectResponse
    {
        $members = User::query()
            ->select(['users.id', 'users.first_name', 'users.last_name', 'users.email'])
            ->whereIn('users.id', $request->safe()->members)
            ->whereDoesntHave('verifiedActions', function ($query) use ($verifiedAction) {
                $query->where('verified_actions.id', $verifiedAction->id);
            })
            ->get();

        foreach ($members as $member) {
            $verifiedAction->verify($member);
        }

        audit_log(
            resource: $verifiedAction,
            event: 'Verified Action Verified',
            summary: "{$request->user()->name} verified the \"{$verifiedAction->name}\" Verified Action for {$members->count()} member(s).",
            data: $members->toJson(),
        );

        return redirect(route('admin.verified-actions.index'))->with('success', 'Member actions successfully verified.');
    }
}
