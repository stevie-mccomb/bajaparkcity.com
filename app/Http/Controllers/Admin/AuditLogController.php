<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\AuditLog\IndexRequest;
use App\Http\Requests\AuditLog\ShowRequest;
use App\Models\AuditLogEvent;
use App\Http\Controllers\Controller;
use Illuminate\View\View;

class AuditLogController extends Controller
{
    /**
     * Display the audit log.
     */
    public function index(IndexRequest $request): View
    {
        $query = AuditLogEvent::orderBy('created_at', 'desc');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['auditLogEvents'] = $query->paginate(25);

        return view('admin.audit-log.index', $data);
    }

    /**
     * Display the given event's data.
     */
    public function show(ShowRequest $request, AuditLogEvent $auditLogEvent): View
    {
        $data['auditLogEvent'] = $auditLogEvent;

        return view('admin.audit-log.show', $data);
    }
}
