<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Coupons\CreateRequest;
use App\Http\Requests\Coupons\DeleteRequest;
use App\Http\Requests\Coupons\RedeemRequest;
use App\Http\Requests\Coupons\SendRequest;
use App\Http\Requests\Coupons\StoreRequest;
use App\Http\Requests\Coupons\UpdateRequest;
use App\Models\AuditLogEvent;
use App\Models\Coupon;
use App\Models\CouponRedemption;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CouponController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $query = Coupon::orderBy('updated_at', 'desc');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['coupons'] = $query->paginate(25);

        return view('admin.coupons.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $data['coupon'] = new Coupon;

        return view('admin.coupons.edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRequest $request): RedirectResponse
    {
        $data = $request->safe()->toArray();
        $data['creator_id'] = $request->user()->id;
        if (empty($data['active_at'])) $data['active_at'] = now()->format('Y-m-d\TH:i:s');

        $coupon = Coupon::create($data);

        audit_log(
            resource: $coupon,
            event: 'Coupon Created',
            summary: "{$request->user()->name} created new \"{$coupon->name}\" coupon.",
            data: $coupon->toJson(),
        );

        return redirect(route('admin.coupons.edit', $coupon))->with('success', 'Coupon successfully created.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Coupon $coupon): View
    {
        $data['coupon'] = $coupon;

        return view('admin.coupons.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRequest $request, Coupon $coupon): RedirectResponse
    {
        $coupon->update($request->safe()->toArray());

        $changes = $coupon->getChanges();
        if (!empty($changes)) {
            audit_log(
                resource: $coupon,
                event: 'Coupon Updated',
                summary: "{$request->user()->name} updated \"{$coupon->name}\" coupon.",
                data: json_encode($changes),
            );
        }

        return redirect(route('admin.coupons.edit', $coupon))->with('success', 'Coupon successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(DeleteRequest $request, Coupon $coupon): RedirectResponse
    {
        $coupon->delete();

        audit_log(
            resource: $coupon,
            event: 'Coupon Deleted',
            summary: "{$request->user()->name} deleted \"{$coupon->name}\" coupon.",
        );

        return redirect(route('admin.coupons.index'))->with('success', 'Coupon successfully archived.');
    }

    /**
     * Display the form for redeeming coupons for members.
     */
    public function getRedeem(Coupon $coupon): View
    {
        $data['coupon'] = $coupon;

        return view('admin.coupons.redeem', $data);
    }

    /**
     * Display the form for redeeming coupons for members.
     */
    public function postRedeem(RedeemRequest $request, Coupon $coupon): RedirectResponse
    {
        foreach ($request->safe()->members as $memberId) {
            $member = User::where('id', $memberId)->first();

            $couponRedemption = CouponRedemption::create([
                'coupon_id' => $coupon->id,
                'user_id' => $member->id,
            ]);

            audit_log(
                resource: $coupon,
                event: 'Coupon Redeemed',
                summary: "{$request->user()->name} redeemed coupon {$coupon->name} for member {$member->name}.",
                data: $couponRedemption->toJson(),
            );
        }

        return redirect(route('admin.coupons.index'))->with('success', 'Coupons successfully redeemed.');
    }

    /**
     * Display the form for sending coupons to members.
     */
    public function getSend(Coupon $coupon): View
    {
        $data['coupon'] = $coupon;

        return view('admin.coupons.send', $data);
    }

    /**
     * Send the given coupon to the given members.
     */
    public function postSend(SendRequest $request, Coupon $coupon): RedirectResponse
    {
        $coupon->users()->attach($request->safe()->members);

        return redirect(route('admin.coupons.index'))->with('success', 'Coupon successfully sent to members.');
    }
}
