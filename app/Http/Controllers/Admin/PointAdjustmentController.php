<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\PointAdjustments\CreateRequest;
use App\Http\Requests\PointAdjustments\CreateFromQrCodeRequest;
use App\Http\Controllers\Controller;
use App\Models\PointAdjustment;
use App\Models\User;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PointAdjustmentController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $query = PointAdjustment::orderBy('created_at', 'desc');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['pointAdjustments'] = $query->paginate(25);

        return view('admin.point-adjustments.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        return view('admin.point-adjustments.create');
    }

    /**
     * Display the confirmation page for a manager to confirm spending a customer's points from a QR code.
     */
    public function createFromQrCode(CreateFromQrCodeRequest $request, User $user): View
    {
        $data['member'] = $user;
        $data['value'] = $request->safe()->value;

        return view('admin.point-adjustments.create-from-qr-code', $data);
    }

    /**
     * Create an automatic point adjustment for the given request details.
     */
    public function storeFromQrCode(CreateFromQrCodeRequest $request, User $user): RedirectResponse
    {
        $pointAdjustment = PointAdjustment::create([
            'adjuster_id' => $request->user()->id,
            'user_id' => $user->id,
            'value' => $request->safe()->value * -1, // The value given here is a deduction, so it should be negative.
            'reason' => 'Points redeemed by member.',
        ]);

        audit_log(
            resource: $pointAdjustment,
            event: 'Point Adjustment Created',
            summary: "{$request->user()->name} adjusted {$pointAdjustment->user->name}'s points by {$request->safe()->value}.",
            data: $pointAdjustment->toJson(),
        );

        return redirect(route('admin.point-adjustments.index'))->with('success', 'Point Adjustment successfully created. Make sure to discount the customer\'s order by the appropriate dollar value: ' . pointsToDollars($request->safe()->value));
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CreateRequest $request): RedirectResponse
    {
        $pointAdjustment = PointAdjustment::create([
            'adjuster_id' => $request->user()->id,
            'user_id' => $request->safe()->member,
            ...$request->safe([ 'value', 'reason' ]),
        ]);

        audit_log(
            resource: $pointAdjustment,
            event: 'Point Adjustment Created',
            summary: "{$request->user()->name} adjusted {$pointAdjustment->user->name}'s points by {$request->safe()->value}.",
            data: $pointAdjustment->toJson(),
        );

        return redirect(route('admin.point-adjustments.index'))->with('success', 'Point Adjustment successfully created.');
    }
}
