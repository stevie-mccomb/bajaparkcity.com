<?php

namespace App\Http\Controllers;

use App\Models\Coupon;
use Illuminate\Http\Request;
use Illuminate\View\View;

class DashboardController extends Controller
{
    /**
     * Display the user's Cantina Club dashboard.
     */
    public function dashboard(): View
    {
        $data['coupons'] = Coupon::eligible()->get();

        return view('cantina-club.dashboard', $data);
    }
}
