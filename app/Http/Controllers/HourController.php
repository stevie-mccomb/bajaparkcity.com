<?php

namespace App\Http\Controllers;

use Storage;
use Illuminate\Http\File;
use Illuminate\Http\Request;

class HourController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $data['hours'] = Storage::exists('hours.json') ? json_decode(Storage::get('hours.json')) : (Object) [];

        return view('hours.show', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $data['hours'] = Storage::exists('hours.json') ? Storage::get('hours.json') : '{}';

        return view('admin.hours.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        Storage::put('hours.json', $request->input('hours'));

        audit_log(
            resource: $request->user(),
            event: 'Adjusted Website Hours',
            summary: "{$request->user()->name} adjusted website hours.",
            data: $request->input('hours'),
        );

        return back()->with('success', 'Hours successfully updated.');
    }
}
