<?php

namespace App\Http\Controllers;

use App\Models\Role;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request): View
    {
        $query = User::orderBy('first_name');

        if ($request->filled('search')) {
            $query->search($request->input('search'));
        }

        $data['users'] = $query->paginate(25);

        return view('admin.users.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(): View
    {
        $data['user'] = new User;
        $data['roles'] = Role::all();

        return view('admin.users.edit', $data);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request): RedirectResponse
    {
        $this->validate($request, [
            'role_id' => 'nullable|integer|exists:roles,id',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|email|unique:users,email',
            'phone' => 'nullable|string|max:32',
            'new_password' => 'required|string|min:8|confirmed',
        ]);

        $user = User::create([
            'role_id' => $request->input('role_id') ?? null,
            'first_name' => $request->input('first_name'),
            'last_name' => $request->input('last_name'),
            'email' => $request->input('email'),
            'phone' => $request->input('phone') ?? null,
            'password' => $request->filled('new_password') ? bcrypt($request->input('new_password')) : null,
        ]);

        audit_log(
            resource: $user,
            event: 'User Created',
            summary: "{$request->user()->name} created new user: \"{$user->name}\"",
            data: $user->toJson(),
        );

        return redirect(route('admin.users.edit', $user))->with('success', 'User successfully updated.');
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user): View
    {
        $data['user'] = $user;
        $data['roles'] = Role::all();

        return view('admin.users.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, User $user): RedirectResponse
    {
        $this->validate($request, [
            'role_id' => 'nullable|integer|exists:roles,id',
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => "required|email|unique:users,email,{$user->id}",
            'phone' => 'nullable|string|max:32',
            'new_password' => 'nullable|string|min:8|confirmed',
        ]);

        $data = $request->only([
            'role_id',
            'first_name',
            'last_name',
            'email',
            'phone',
            'new_password',
        ]);

        if (!empty($data['new_password'])) {
            $data['password'] = bcrypt($data['new_password']);
            unset($data['new_password']);
        }

        $user->update($data);

        $changes = $user->getChanges();
        if (!empty($changes)) {
            if (!empty($changes['password'])) {
                $changes['password'] = '********';
            }
            audit_log(
                resource: $user,
                event: 'User Updated',
                summary: "{$request->user()->name} updated \"{$user->name}\".",
                data: $changes,
            );
        }

        return redirect(route('admin.users.edit', $user))->with('success', 'User successfully updated.');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, User $user): RedirectResponse
    {
        $this->authorize('manage-users');

        $user->delete();

        audit_log(
            resource: $user,
            event: 'User Deleted',
            summary: "{$request->user()->name} deleted {$user->name}.",
        );

        return redirect(route('admin.users.index'))->with('success', 'User successfully deleted.');
    }

    /**
     * Return an index of Cantina Club members.
     */
    public function asyncIndexCantinaClubMembers(): JsonResponse
    {
        $this->authorize('view-users');

        return response()->json(
            User::query()
                ->doesntHave('role')
                ->orderBy('users.first_name')
                ->orderBy('users.last_name')
                ->orderBy('users.email')
                ->get()
        );
    }
}
