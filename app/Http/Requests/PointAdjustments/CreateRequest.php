<?php

namespace App\Http\Requests\PointAdjustments;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('manage-points');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'member' => 'required|integer|exists:users,id',
            'value' => 'required|integer|min:-9999|max:9999',
            'reason' => 'required|string|max:255',
        ];
    }
}
