<?php

namespace App\Http\Requests\PointAdjustments;

use App\Models\User;
use Illuminate\Foundation\Http\FormRequest;

class CreateFromQrCodeRequest extends FormRequest
{
    /**
     * The route to redirect to if validation fails.
     *
     * @var string
     */
    protected $redirectRoute = 'admin.point-adjustments.index';

    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return $this->user()->can('manage-points');
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'value' => [
                'required',
                'integer',
                'min:1',
                function ($attribute, $value, $fail) {
                    if ($value > 2000) {
                        $fail("No more than 2000 Pico Points may be spent in a single transaction.");
                    }

                    $member = $this->route('user');
                    if ($value > $member->points) {
                        $fail("This transaction of {$value} points would exceed the member's current balance of {$member->points} points.");
                    }
                }
            ],
        ];
    }
}
