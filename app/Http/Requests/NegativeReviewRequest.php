<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class NegativeReviewRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array|string>
     */
    public function rules(): array
    {
        return [
            'experience' => 'required|in:negative',

            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|max:255',
            'phone' => 'nullable|string|max:32',

            'when' => 'required|string|max:255',
            'server' => 'nullable|string|max:255',
            'spoke_to_manager' => 'required|in:yes,no',
            'manager' => 'nullable|string|max:255',
            'other_staff_involved' => 'required|in:yes,no',
            'other_staff' => 'nullable|string|max:255',
            'details' => 'required|string',
        ];
    }
}
