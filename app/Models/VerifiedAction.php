<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\Cache;

class VerifiedAction extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The cast map for the model.
     */
    protected $casts = [
        'automatic' => 'boolean',
    ];

    /**
     * The attributes that are mass-assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'description',
        'value',
        'url',
        'automatic',
    ];

    /**
     * Return the users that have completed this verified action.
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'users_verified_actions', 'verified_action_id', 'user_id');
    }

    /**
     * Return whether this user has completed this action or not.
     */
    public function getVerifiedAttribute(): bool
    {
        $userId = auth()->user()->id;

        return Cache::rememberForever(
            key: "user:{$userId}.verified-actions.{$this->id}.verified",
            callback: fn () => $this->users()->where('users.id', auth()->user()->id)->exists(),
        );
    }

    /**
     * Return verified actions matching the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function ($query) use ($value) {
            $query->where('verified_actions.name', 'like', $value);
            $query->orWhere('verified_actions.description', 'like', $value);
        });
    }

    /**
     * The validation rules for creating/updating verified actions.
     */
    public function validationRules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'description' => 'required|string|max:255',
            'url' => 'nullable|url:https,http',
            'value' => 'required|integer|min:1|max:100',
            'automatic' => 'nullable|boolean',
        ];
    }

    /**
     * Attach this action to the given user and award them their points.
     */
    public function verify(User $user): bool
    {
        $alreadyAttached = $this->users()->where('users.id', $user->id)->exists();

        if (!$alreadyAttached) {
            $this->users()->attach($user);

            PointAdjustment::create([
                'user_id' => $user->id,
                'value' => $this->value,
                'reason' => "Automatic verified action: {$this->name}",
            ]);

            Cache::forget("user:{$user->id}.verified-actions.{$this->id}.verified");

            return true;
        }

        return false;
    }
}
