<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\MorphTo;
use Illuminate\Support\Facades\DB;

class AuditLogEvent extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass-assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'auditable_type',
        'auditable_id',
        'user_id',
        'event',
        'summary',
        'data',
    ];

    /**
     * Register listeners for this model's own event reactions.
     */
    protected static function booted(): void
    {
        static::creating(function (self $auditLogEvent) {
            if (empty($auditLogEvent->user_id) && auth()->check()) {
                $auditLogEvent->user_id = auth()->user()->id;
            }
        });
    }

    /**
     * Return the thing that this audit event occurred on (user, menu, coupon, etc.).
     */
    public function auditable(): MorphTo
    {
        return $this->morphTo();
    }

    /**
     * Return the user whose actions created this audit log event.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Return audit log events matching the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function ($query) use ($value) {
            $query->where('audit_log_events.event', 'like', $value);
            $query->orWhere('audit_log_events.summary', 'like', $value);
            $query->orWhere('audit_log_events.data', 'like', $value);
            $query->orWhereHas('user', function ($query) use ($value) {
                $query->where(DB::raw("CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)"), 'like', $value);
                $query->orWhere('users.email', 'like', $value);
            });
        });
    }
}
