<?php

namespace App\Models;

use App\Events\PointAdjustmentCreated;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Facades\DB;

class PointAdjustment extends Model
{
    use HasFactory;

    /**
     * The event map for the model.
     *
     * @var array<string, string>
     */
    protected $dispatchesEvents = [
        'created' => PointAdjustmentCreated::class,
    ];

    /**
     * The attributes that are mass-assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'adjuster_id',
        'user_id',
        'value',
        'reason',
    ];

    /**
     * Return the user that submitted this point adjustment.
     */
    public function adjuster(): BelongsTo
    {
        return $this->belongsTo(User::class, 'adjuster_id');
    }

    /**
     * Return the user whose points were adjusted.
     */
    public function user(): BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * Return point adjustments matching the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function ($query) use ($value) {
            $query->whereHas('adjuster', function ($query) use ($value) {
                $query->where(DB::raw('CONCAT(`users`.`first_name`, " ", `users`.`last_name`)'), 'like', $value);
                $query->orWhere('users.email', 'like', $value);
            });

            $query->orWhereHas('user', function ($query) use ($value) {
                $query->where(DB::raw('CONCAT(`users`.`first_name`, " ", `users`.`last_name`)'), 'like', $value);
                $query->orWhere('users.email', 'like', $value);
            });

            $query->orWhere('point_adjustments.reason', 'like', $value);
        });
    }
}
