<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphMany;
use Illuminate\Database\Eloquent\SoftDeletes;

class Coupon extends Model
{
    use HasFactory, SoftDeletes;

    /**
     * The cast map for the model.
     */
    protected $casts = [
        'active_at' => 'datetime:Y-m-d\TH:i:s',
        'expires_at' => 'datetime:Y-m-d\TH:i:s',
        'public' => 'boolean',
    ];

    /**
     * The attributes that are mass-assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'code',
        'description',
        'terms',
        'public',
        'active_at',
        'expires_at',
    ];

    /**
     * Return the redemptions that have been submitted for this coupon.
     * Each redemption is an instance of a customer redeeming this coupon (only allowed once per customer).
     */
    public function redemptions(): HasMany
    {
        return $this->hasMany(CouponRedemption::class);
    }

    /**
     * Return the users that have been attached to this coupon as part of a special promotion.
     */
    public function users(): BelongsToMany
    {
        return $this->belongsToMany(User::class, 'coupons_users', 'coupon_id', 'user_id');
    }

    /**
     * Retrieve coupons that are currently active (activated and not yet expired).
     */
    public function scopeActive(Builder $query): Builder
    {
        $now = now();

        return $query
            ->where('active_at', '<=', $now)
            ->where('expires_at', '>', $now);
    }

    /**
     * Return coupons that are attached to the given user (defaults to the currently logged-in user.)
     */
    public function scopeAttached(Builder $query, User $user = null): Builder
    {
        if (empty($user)) $user = auth()->user();
        if (empty($user)) return $query->whereRaw('false');

        return $query->whereHas('users', function ($query) use ($user) {
            $query->where('users.id', $user->id);
        });
    }

    /**
     * Return coupons that the current user is eligible to redeem.
     */
    public function scopeEligible(Builder $query): Builder
    {
        return $query
            ->active()
            ->unredeemed()
            ->where(function ($query) {
                $query->where(fn ($query) => $query->attached());
                $query->orWhere(fn ($query) => $query->public());
            });
    }

    /**
     * Retrieve coupons that are marked as public (available to all Cantina Club members).
     */
    public function scopePublic(Builder $query): Builder
    {
        return $query->where('public', 1);
    }

    /**
     * Retrieve coupons that the current user has redeemed.
     */
    public function scopeRedeemed(Builder $query): Builder
    {
        return $query->whereHas('redemptions', function ($query) {
            $query->where('coupon_redemptions.user_id', auth()->user()->id);
        });
    }

    /**
     * Return coupons matching the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function ($query) use ($value) {
            $query->where('coupons.name', 'like', $value);
            $query->orWhere('coupons.code', 'like', $value);
            $query->orWhere('coupons.description', 'like', $value);
        });
    }

    /**
     * Retrieve coupons that the current user has not yet redeemed.
     */
    public function scopeUnredeemed(Builder $query): Builder
    {
        return $query->whereDoesntHave('redemptions', function ($query) {
            $query->where('coupon_redemptions.user_id', auth()->user()->id);
        });
    }

    /**
     * The validation rules for creating/updating coupons.
     */
    public function validationRules(): array
    {
        return [
            'name' => 'required|string|max:255',
            'code' => 'nullable|string|max:255',
            'description' => 'required|string|max:255',
            'terms' => 'nullable|string',
            'active_at' => 'nullable|date_format:Y-m-d\TH:i:s',
            'expires_at' => 'required|date_format:Y-m-d\TH:i:s',
            'public' => 'required|boolean',
        ];
    }
}
