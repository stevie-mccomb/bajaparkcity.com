<?php

namespace App\Models;

use stdClass;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class Menu extends Model
{
    use HasFactory;

    /**
     * The attributes that are mass-assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'slug',
        'content',
        'sort_order',
    ];

    /**
     * Return menus matching the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function ($query) use ($value) {
            $query->where('menus.name', 'like', $value);
            $query->orWhere('menus.slug', 'like', $value);
            $query->orWhere('menus.content', 'like', $value);
        });
    }

    /**
     * JSON decode the menu's content.
     */
    public function getContentAttribute($value): stdClass
    {
        $content = json_decode($value) ?? (object) [];

        return $content;
    }
}
