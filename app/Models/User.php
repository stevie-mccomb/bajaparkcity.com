<?php

namespace App\Models;

use NumberFormatter;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;

class User extends Authenticatable implements MustVerifyEmail
{
    use HasFactory, Notifiable;

    /**
     * The accessors to always run when querying resources.
     */
    protected $appends = [
        'name',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'first_name',
        'last_name',
        'email',
        'phone',
        'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Return the coupons that have been attached to this user as part of a special promotion.
     */
    public function coupons(): BelongsToMany
    {
        return $this->belongsToMany(Coupon::class, 'coupons_users', 'user_id', 'coupon_id');
    }

    /**
     * Return the point adjustments that have been made to this user's point balance.
     */
    public function pointAdjustments(): HasMany
    {
        return $this->hasMany(PointAdjustment::class);
    }

    /**
     * Returns this user's role.
     */
    public function role(): BelongsTo
    {
        return $this->belongsTo(Role::class);
    }

    /**
     * Return the verified actions that this user has completed.
     */
    public function verifiedActions(): BelongsToMany
    {
        return $this->belongsToMany(VerifiedAction::class, 'users_verified_actions', 'user_id', 'verified_action_id');
    }

    /**
     * Return this user's full name (first and last).
     */
    public function getNameAttribute(): string
    {
        return "{$this->first_name} {$this->last_name}";
    }

    /**
     * Return a sum of this user's point adjustment values.
     */
    public function getPointsAttribute(): int
    {
        return Cache::rememberForever(
            key: "user:{$this->id}.points",
            callback: fn () => $this->pointAdjustments()->sum('value') ?? 0,
        );
    }

    /**
     * Pico points are worth 5 cents each.
     * This uses that conversion rate to convert points into a dollar amount.
     */
    public function getPointsToDollarsAttribute(): string
    {
        return pointsToDollars($this->points);
    }

    /**
     * Return users that match the given search string.
     */
    public function scopeSearch(Builder $query, string $value): Builder
    {
        $value = "%{$value}%";

        return $query->where(function($query) use ($value) {
            $query->where(DB::raw('CONCAT(`users`.`first_name`, " ", `users`.`last_name`)'), 'like', $value);
            $query->orWhere('users.email', 'like', $value);
            $query->orWhere('users.phone', 'like', $value);
        });
    }

    /**
     * Returns whether or not this user has the given role.
     */
    public function hasRole($slug = 'guest'): bool
    {
        return ($this->role->slug ?? 'guest') === $slug;
    }
}
