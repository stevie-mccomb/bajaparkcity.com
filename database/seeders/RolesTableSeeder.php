<?php

namespace Database\Seeders;

use DB;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement("
            INSERT INTO
                `roles` (`id`, `name`, `slug`)
            VALUES
                (1, 'Administrator', 'admin'),
                (2, 'Manager', 'manager')
            ON DUPLICATE KEY UPDATE
                `name` = VALUES(`name`),
                `slug` = VALUES(`slug`);
        ");
    }
}
