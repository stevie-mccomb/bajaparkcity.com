<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->dropForeign('menus_creator_id_foreign');
            $table->dropForeign('menus_updater_id_foreign');
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->dropColumn('creator_id');
            $table->dropColumn('updater_id');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('menus', function (Blueprint $table) {
            $table->unsignedBigInteger('creator_id')->nullable();
            $table->unsignedBigInteger('updater_id')->nullable();
        });

        Schema::table('menus', function (Blueprint $table) {
            $table->foreign('creator_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('updater_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
        });
    }
};
