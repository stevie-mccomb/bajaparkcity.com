<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('first_name')->after('name');
            $table->string('last_name')->after('first_name');
        });

        DB::statement("
            UPDATE
                `users`
            SET
                `users`.`first_name` = SUBSTRING_INDEX(`users`.`name`, ' ', 1),
                `users`.`last_name` = TRIM(REPLACE(`users`.`name`, SUBSTRING_INDEX(`users`.`name`, ' ', 1), ''))
        ");

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('name');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('name')->after('role_id');
        });

        DB::statement("
            UPDATE
                `users`
            SET
                `users`.`name` = CONCAT(`users`.`first_name`, ' ', `users`.`last_name`)
        ");

        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn('first_name');
            $table->dropColumn('last_name');
        });
    }
};
