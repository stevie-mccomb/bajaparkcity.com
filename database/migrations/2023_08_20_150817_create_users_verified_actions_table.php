<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_verified_actions', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('user_id');
            $table->unsignedBigInteger('verified_action_id');
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrent();
        });

        Schema::table('users_verified_actions', function (Blueprint $table) {
            $table->unique([ 'user_id', 'verified_action_id' ]);

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('verified_action_id')->references('id')->on('verified_actions')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('users_verified_actions', function (Blueprint $table) {
            $table->dropForeign('users_verified_actions_user_id_foreign');
            $table->dropForeign('users_verified_actions_verified_action_id_foreign');
        });

        Schema::dropIfExists('users_verified_actions');
    }
};
