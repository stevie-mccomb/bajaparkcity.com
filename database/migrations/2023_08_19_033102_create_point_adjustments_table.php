<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('point_adjustments', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('adjuster_id')->nullable();
            $table->unsignedBigInteger('user_id');
            $table->integer('value');
            $table->string('reason');
            $table->dateTime('created_at')->useCurrent();
            $table->dateTime('updated_at')->useCurrent();
        });

        Schema::table('point_adjustments', function (Blueprint $table) {
            $table->foreign('adjuster_id')->references('id')->on('users')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::table('point_adjustments', function (Blueprint $table) {
            $table->dropForeign('point_adjustments_adjuster_id_foreign');
            $table->dropForeign('point_adjustments_user_id_foreign');
        });

        Schema::dropIfExists('point_adjustments');
    }
};
