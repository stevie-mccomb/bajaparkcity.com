<?php

namespace Database\Factories;

use stdClass;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Menu>
 */
class MenuFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => Str::random(32),
            'slug' => Str::random(32),
            'content' => json_encode($this->_mockContent()),
            'sort_order' => fake()->randomNumber(),
        ];
    }

    /**
     * Make a fake menu content class.
     */
    private function _mockContent(int $countSections = 3, int $countItems = 3): stdClass
    {
        $content = (object) [
            'id' => Str::random(8),
            'sections' => [],
        ];

        for ($i = 0; $i < $countSections; ++$i) {
            $content->sections[] = $this->_mockSection($countItems);
        }

        return $content;
    }

    /**
     * Make a fake menu section.
     */
    private function _mockSection(int $countItems = 3): stdClass
    {
        $section = (object) [
            'id' => Str::random(8),
            'name' => fake()->words(3, true),
            'items' => [],
        ];

        for ($i = 0; $i < $countItems; ++$i) {
            $section->items[] = $this->_mockItem();
        }

        return $section;
    }

    /**
     * Make a fake menu item.
     */
    private function _mockItem(): stdClass
    {
        return (object) [
            'id' => Str::random(8),
            'name' => fake()->words(3, true),
            'price' => fake()->randomFloat(2),
            'description' => fake()->sentence(),
        ];
    }
}
