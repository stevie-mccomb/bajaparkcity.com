<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\PointAdjustment>
 */
class PointAdjustmentFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'adjuster_id' => User::factory(),
            'user_id' => User::factory(),
            'value' => fake()->numberBetween(1, 100),
            'reason' => fake()->sentence(),
        ];
    }

    /**
     * Create a system point adjustment with no adjuster.
     */
    public function system(): static
    {
        return $this->state(function (array $attributes) {
            return [
                'adjuster_id' => null,
            ];
        });
    }
}
