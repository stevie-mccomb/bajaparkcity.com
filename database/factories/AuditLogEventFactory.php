<?php

namespace Database\Factories;

use App\Models\Coupon;
use App\Models\PointAdjustment;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\AuditLogEvent>
 */
class AuditLogEventFactory extends Factory
{
    /**
     * The models that may be auditable
     */
    private $_auditableTypes = [
        Coupon::class,
        PointAdjustment::class,
        User::class,
    ];

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $type = collect($this->_auditableTypes)->random();

        return [
            'auditable_type' => $type,
            'auditable_id' => $type::factory(),
            'user_id' => User::factory(),
            'event' => fake()->words(3, true),
            'summary' => fake()->sentence(),
        ];
    }

    /**
     * Create an audit log event with example data in it.
     */
    public function withData(): static
    {
        return $this->state(function (array $attributes) {
            return [
                'data' => json_encode([ 'example' => 'This is an example.' ]),
            ];
        });
    }
}
