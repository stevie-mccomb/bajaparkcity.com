<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Coupon>
 */
class CouponFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'name' => fake()->words(3, true),
            'code' => strtoupper(implode('-', fake()->words(3))),
            'description' => fake()->sentence(),
            'terms' => fake()->sentence(),
            'public' => fake()->boolean(),
            'active_at' => now()->subMinutes(30),
            'expires_at' => now()->addMinutes(60),
        ];
    }

    /**
     * Create a coupon that expired 30 minutes ago.
     */
    public function expired(): static
    {
        return $this->state(function (array $attributes) {
            return [
                'expires_at' => now()->subMinutes(30),
            ];
        });
    }
}
