<?php

namespace Tests\Unit\Helpers;

use Throwable;
use App\Models\AuditLogEvent;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Tests\TestCase;

class AuditLogTest extends TestCase
{
    use RefreshDatabase;

    public function test_audit_log_helper_creates_an_audit_log_event(): void
    {
        $data = AuditLogEvent::factory()->withData()->make()->toArray();

        audit_log(
            resource: $data['auditable_type']::find($data['auditable_id']),
            event: $data['event'],
            summary: $data['summary'],
            data: $data['data'],
        );

        $auditLogEvent = AuditLogEvent::where('event', $data['event'])->first();
        $this->assertModelExists($auditLogEvent);
        $this->assertSame($auditLogEvent->event, $data['event']);
        $this->assertSame($auditLogEvent->summary, $data['summary']);
        $this->assertSame(
            json_encode(json_decode($auditLogEvent->data)),
            json_encode(json_decode($data['data'])),
        );
    }

    public function test_audit_logs_fill_their_own_user_id_when_not_provided(): void
    {
        $user = User::factory()->create();

        $data = AuditLogEvent::factory()->make()->toArray();
        unset($data['user_id']);

        $this->actingAs($user);

        $auditLogEvent = AuditLogEvent::create($data);
        $this->assertSame($auditLogEvent->user_id, $user->id);
    }
}
