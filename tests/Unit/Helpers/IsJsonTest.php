<?php

namespace Tests\Unit\Helpers;

use PHPUnit\Framework\TestCase;

class IsJsonTest extends TestCase
{
    public function test_is_json_detects_valid_json(): void
    {
        $validJson = json_encode([ 'example' => 'This is an example.' ]);
        $this->assertTrue(is_json($validJson));
    }

    public function test_is_json_detects_invalid_json(): void
    {
        $invalidJson = '[object Object]';
        $this->assertFalse(is_json($invalidJson));
    }

    public function test_is_json_fails_gracefully(): void
    {
        $invalidJson = ['Example']; // non-string input
        $this->assertFalse(is_json($invalidJson));
    }
}
