<?php

namespace Tests\Unit\Libraries;

use App\Libraries\ActiveRoute;
use Illuminate\Support\Facades\Request;
use Mockery;
use Tests\TestCase;

class ActiveRouteTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->get('/path/to/example');
    }

    public function test_is(): void
    {
        $this->assertSame(ActiveRoute::is('/path/to/example'), 'active');
    }

    public function test_is_fails_gracefully(): void
    {
        $this->assertSame(ActiveRoute::is(null), '');
    }

    public function test_starts_with(): void
    {
        $this->assertSame(ActiveRoute::startsWith('path/to'), 'active');
    }

    public function test_starts_with_fails_gracefully(): void
    {
        $this->assertSame(ActiveRoute::startsWith(null), '');
    }

    public function test_ends_with(): void
    {
        $this->assertSame(ActiveRoute::endsWith('example'), 'active');
    }

    public function test_ends_with_fails_gracefully(): void
    {
        $this->assertSame(ActiveRoute::endsWith(null), '');
    }

    public function test_not(): void
    {
        $this->assertSame(ActiveRoute::not('example'), 'active');
    }

    public function test_not_fails_gracefully(): void
    {
        $this->assertSame(ActiveRoute::not(null), 'active');
    }
}
