<?php

namespace Tests\Unit\Rules;

use App\Rules\Recaptcha;
use Illuminate\Support\Facades\Http;
use Tests\TestCase;

class RecaptchaTest extends TestCase
{
    public function test_valid_recaptcha_passes(): void
    {
        Http::fake([
            'https://www.google.com/recaptcha/api/siteverify' => Http::response([ 'score' => 1.0 ], 200),
        ]);

        config(['app.env' => 'local']);

        (new Recaptcha)->validate('g-recaptcha-response', 'example', function () {
            $this->assertTrue(false);
        });

        config(['app.env' => 'testing']);
    }

    public function test_invalid_recaptcha_fails(): void
    {
        Http::fake([
            'https://www.google.com/recaptcha/api/siteverify' => Http::response([ 'score' => 0.0 ], 200),
        ]);

        config(['app.env' => 'local']);

        (new Recaptcha)->validate('g-recaptcha-response', 'example', function () {
            $this->assertTrue(true);
        });

        config(['app.env' => 'testing']);
    }
}
