<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;
use Illuminate\Mail\MailManager;
use Illuminate\Support\Facades\Mail;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    /**
     * Should seed the database.
     */
    protected $seed = true;

    /**
     * Save a reference to the original mail facade so it can be restored when making assertions about faked mailables.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->realMailer = Mail::getFacadeRoot();
        $this->assertTrue($this->realMailer instanceof MailManager);
    }

    /**
     * Retrieve the real mailable when faking mail. Useful for making assertions about the email's content for 100% testing coverage.
     * Unfortunately, this "real" mailable will not contain the properly envelope, but the content will be rendered.
     */
    public function getRealMailable(string $mailableClass, callable $callback): void
    {
        Mail::assertSent($mailableClass, function ($mail) use ($callback): bool {
            $existingMailer = Mail::getFacadeRoot();

            Mail::swap($this->realMailer);

            $callback($mail);

            Mail::swap($existingMailer);

            return true;
        });
    }
}
