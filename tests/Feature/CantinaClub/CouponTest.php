<?php

namespace Tests\Feature\CantinaClub;

use App\Models\Coupon;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();

        $this->publicCoupon = Coupon::factory()->create([ 'public' => true ]);
        $this->attachedCoupon = Coupon::factory()->hasAttached($this->user)->create([ 'public' => false, ]);
        $this->hiddenCoupon = Coupon::factory()->create([ 'public' => false ]);
    }

    public function test_member_can_only_see_appropriate_coupons_on_coupons_screen(): void
    {
        $response = $this->actingAs($this->user)->get(route('cantina-club.coupons'));
        $response->assertStatus(200);
        $response->assertSee($this->publicCoupon->description);
        $response->assertSee($this->attachedCoupon->description);
        $response->assertDontSee($this->hiddenCoupon->description);
    }

    public function test_member_can_only_see_appropriate_coupons_on_dashboard(): void
    {
        $response = $this->actingAs($this->user)->get(route('cantina-club'));
        $response->assertStatus(200);
        $response->assertSee($this->publicCoupon->description);
        $response->assertSee($this->attachedCoupon->description);
        $response->assertDontSee($this->hiddenCoupon->description);
    }
}
