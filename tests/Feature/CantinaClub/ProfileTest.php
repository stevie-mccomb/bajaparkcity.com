<?php

namespace Tests\Feature\CantinaClub;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class ProfileTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    public function test_user_can_view_edit_profile_form(): void
    {
        $response = $this->actingAs($this->user)->get(route('cantina-club.profile'));
        $response->assertStatus(200);
        $response->assertSee($this->user->first_name);
        $response->assertSee($this->user->last_name);
        $response->assertSee($this->user->email);
        if (!empty($this->user->phone)) $response->assertSee($this->user->phone);
    }

    public function test_user_can_update_profile(): void
    {
        $data = User::factory()->make()->toArray();
        $data['new_password'] = Str::random(32);
        $data['new_password_confirmation'] = $data['new_password'];
        $data['phone'] = '(555) 555-5555';

        $response = $this->actingAs($this->user)->patch(route('cantina-club.profile.update'), $data);
        $response->assertRedirect(route('cantina-club.profile'));

        $this->assertSame($this->user->first_name, $data['first_name']);
        $this->assertSame($this->user->last_name, $data['last_name']);
        $this->assertSame($this->user->email, $data['email']);
        $this->assertSame($this->user->phone, $data['phone']);

        $this->assertTrue(auth()->attempt([ 'email' => $data['email'], 'password' => $data['new_password'] ]));
    }
}
