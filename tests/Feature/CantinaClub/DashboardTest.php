<?php

namespace Tests\Feature\CantinaClub;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class DashboardTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    public function test_only_verified_users_can_see_dashboard(): void
    {
        $unverifiedUser = User::factory()->unverified()->create();
        $response = $this->actingAs($unverifiedUser)->get(route('cantina-club'));
        $response->assertStatus(302);
        $response->assertSee('/verify-email');

        $response = $this->actingAs($this->user)->get(route('cantina-club'));
        $response->assertStatus(200);
        $response->assertSee('Dashboard');
    }
}
