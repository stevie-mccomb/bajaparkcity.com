<?php

namespace Tests\Feature\CantinaClub;

use App\Models\PointAdjustment;
use App\Models\User;
use App\Models\VerifiedAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\DB;
use Tests\TestCase;

class PointTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create();
    }

    public function test_dashboard_points_are_cached(): void
    {
        // Ensure the user's visit to their dashboard caches their point value.
        $originalPointAdjustment = PointAdjustment::factory()->create([ 'user_id' => $this->user->id ]);
        $response = $this->actingAs($this->user)->get(route('cantina-club'));
        $response->assertSee("<span class=\"point-value-number\">{$originalPointAdjustment->value}</span> Pico Points", escape: false);

        // Ensure that point adjustments manually made in the database don't affect the value (since they shouldn't clear the cache).
        $forcedPointAdjustment = PointAdjustment::factory()->make([ 'user_id' => $this->user->id ]);
        DB::table('point_adjustments')->insert($forcedPointAdjustment->toArray());
        $response = $this->actingAs($this->user)->get(route('cantina-club'));
        $response->assertSee("<span class=\"point-value-number\">{$originalPointAdjustment->value}</span> Pico Points", escape: false);

        // Ensure that making another valid adjustment clears the cache and recalculates all three combined values.
        $secondPointAdjustment = PointAdjustment::factory()->create([ 'user_id' => $this->user->id ]);
        $combinedPoints = $originalPointAdjustment->value + $forcedPointAdjustment->value + $secondPointAdjustment->value;
        $response = $this->actingAs($this->user)->get(route('cantina-club'));
        $response->assertSee("<span class=\"point-value-number\">{$combinedPoints}</span> Pico Points", escape: false);
    }

    public function test_member_can_view_points_total(): void
    {
        $pointAdjustment = PointAdjustment::factory()->create([ 'user_id' => $this->user->id ]);

        $response = $this->actingAs($this->user)->get(route('cantina-club.points'));
        $response->assertStatus(200);
        $response->assertSee($pointAdjustment->value);
    }

    public function test_member_can_view_verified_actions_list(): void
    {
        $verifiedActions = VerifiedAction::factory()->count(3)->create();

        $response = $this->actingAs($this->user)->get(route('cantina-club.points'));
        $response->assertStatus(200);
        $response->assertSee('Make a Purchase');
        foreach ($verifiedActions as $verifiedAction) {
            $response->assertSee($verifiedAction->name);
            $response->assertSee($verifiedAction->description);
            $response->assertSee($verifiedAction->value);
        }
    }

    public function test_member_can_automatically_verify_automatic_actions(): void
    {
        $automaticAction = VerifiedAction::factory()->create([ 'automatic' => true, 'url' => 'https://example.com' ]);
        $manualAction = VerifiedAction::factory()->create([ 'automatic' => false, 'url' => 'https://example.com' ]);

        $response = $this->actingAs($this->user)->get(route('cantina-club.verified-actions.visit', $automaticAction));
        $response->assertRedirect('https://example.com');
        $this->assertTrue($automaticAction->users()->where('users.id', $this->user->id)->exists());

        $response = $this->actingAs($this->user)->get(route('cantina-club.verified-actions.visit', $manualAction));
        $response->assertRedirect('https://example.com');
        $this->assertFalse($manualAction->users()->where('users.id', $this->user->id)->exists());

        $this->assertSame($this->user->points, $automaticAction->value);
    }

    public function test_member_cannot_verify_the_same_automatic_action_twice(): void
    {
        $automaticAction = VerifiedAction::factory()->create([ 'automatic' => true, 'url' => 'https://example.com' ]);

        $this->assertTrue($automaticAction->verify($this->user));
        $this->assertFalse($automaticAction->verify($this->user));
    }
}
