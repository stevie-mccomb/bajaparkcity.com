<?php

namespace Tests\Feature\Admin;

use App\Models\PointAdjustment;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class PointAdjustmentTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->manager = User::factory()->manager()->create();
    }

    public function test_only_managers_can_view_index(): void
    {
        $user = User::factory()->withRole()->create();

        $response = $this->actingAs($user)->get(route('admin.point-adjustments.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->manager)->get(route('admin.point-adjustments.index'));
        $response->assertStatus(200);
    }

    public function test_index_is_searchable(): void
    {
        $pointAdjustment = PointAdjustment::factory()->create();

        $response = $this->actingAs($this->manager)->get(route('admin.point-adjustments.index', [
            'search' => $pointAdjustment->reason,
        ]));

        $response->assertSee($pointAdjustment->value);
        $response->assertSee($pointAdjustment->reason);
    }

    public function test_manager_can_view_create_point_adjustment_form(): void
    {
        $response = $this->actingAs($this->manager)->get(route('admin.point-adjustments.create'));
        $response->assertSee('Create Point Adjustment');
    }

    public function test_manager_can_create_point_adjustment(): void
    {
        $pointAdjustmentData = PointAdjustment::factory()->make()->toArray();
        $member = User::factory()->create();

        $data = [
            ...$pointAdjustmentData,
            'member' => $member->id,
        ];

        $response = $this->actingAs($this->manager)->post(route('admin.point-adjustments.store'), $data);

        $response->assertStatus(302);
        $this->assertTrue(PointAdjustment::where('reason', $pointAdjustmentData['reason'])->exists());
    }

    public function test_manager_can_view_confirmation_for_qr_code_point_redemptions(): void
    {
        $data['user'] = User::factory()->create();
        $data['value'] = fake()->numberBetween(1, 2000);

        PointAdjustment::create([
            'user_id' => $data['user']->id,
            'value' => $data['value'] + 32,
            'reason' => 'Automated Testing',
        ]);

        $response = $this->actingAs($this->manager)->get(route('admin.point-adjustments.create-from-qr-code', $data));
        $response->assertStatus(200);
        $response->assertSee('Confirm Details');
        $response->assertSee($data['user']->name);
        $response->assertSee($data['user']->email);
        $response->assertSee($data['value']);
    }

    public function test_manager_can_create_point_adjustment_from_qr_code(): void
    {
        $data['user'] = User::factory()->create();
        $data['value'] = fake()->numberBetween(1, 2000);

        PointAdjustment::create([
            'user_id' => $data['user']->id,
            'value' => $data['value'] + 32,
            'reason' => 'Automated Testing',
        ]);

        $response = $this->actingAs($this->manager)->post(route('admin.point-adjustments.store-from-qr-code', $data));
        $response->assertRedirect(route('admin.point-adjustments.index'));
        $response->assertSessionHas('success', function (string $value) use ($data) {
            return strpos($value, pointsToDollars($data['value'])) !== false;
        });

        $this->assertSame($data['user']->points, 32);
    }

    public function test_member_cannot_spend_more_points_than_allowed(): void
    {
        $data['user'] = User::factory()->create();
        $data['value'] = fake()->numberBetween(1, 2000);

        PointAdjustment::create([
            'user_id' => $data['user']->id,
            'value' => $data['value'] - 32,
            'reason' => 'Automated Testing',
        ]);
        $originalPoints = $data['user']->points;

        $response = $this->actingAs($this->manager)->post(route('admin.point-adjustments.store-from-qr-code', $data));
        $response->assertRedirect(route('admin.point-adjustments.index'));
        $response->assertSessionHasErrors([
            'value' => "This transaction of {$data['value']} points would exceed the member's current balance of {$data['user']->points} points.",
        ]);

        $data['value'] = 2500;
        $response = $this->actingAs($this->manager)->post(route('admin.point-adjustments.store-from-qr-code', $data));
        $response->assertRedirect(route('admin.point-adjustments.index'));
        $response->assertSessionHasErrors([
            'value' => "No more than 2000 Pico Points may be spent in a single transaction.",
        ]);

        $this->assertSame($data['user']->points, $originalPoints);
    }
}
