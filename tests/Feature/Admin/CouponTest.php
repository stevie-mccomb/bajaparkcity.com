<?php

namespace Tests\Feature\Admin;

use App\Models\Coupon;
use App\Models\CouponRedemption;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class CouponTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->manager = User::factory()->manager()->create();
    }

    public function test_only_managers_can_view_index(): void
    {
        $user = User::factory()->withRole()->create();
        $response = $this->actingAs($user)->get(route('admin.coupons.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->manager)->get(route('admin.coupons.index'));
        $response->assertStatus(200);
    }

    public function test_index_is_searchable(): void
    {
        $exampleCoupon = Coupon::factory()->create([ 'name' => 'Example Coupon' ]);
        $otherCoupon = Coupon::factory()->create();

        $response = $this->actingAs($this->manager)->get(route('admin.coupons.index', [ 'search' => 'Example Coupon' ]));
        $response->assertSee($exampleCoupon->description);
        $response->assertDontSee($otherCoupon->description);
    }

    public function test_manager_can_view_create_coupon_form(): void
    {
        $response = $this->actingAs($this->manager)->get(route('admin.coupons.create'));
        $response->assertSee('Create Coupon');
    }

    public function test_manager_can_create_a_coupon(): void
    {
        $coupon = Coupon::factory()->make();
        $response = $this->actingAs($this->manager)->post(route('admin.coupons.store'), $coupon->toArray());
        $response->assertStatus(302);
        $this->assertTrue(Coupon::where('name', $coupon->name)->exists());
    }

    public function test_manager_can_view_edit_coupon_form(): void
    {
        $coupon = Coupon::factory()->create();
        $response = $this->actingAs($this->manager)->get(route('admin.coupons.edit', $coupon));
        $response->assertSee('Edit Coupon');
        $response->assertSee($coupon->name);
    }

    public function test_manager_can_update_coupon(): void
    {
        $coupon = Coupon::factory()->create();

        $data = [
            'name' => fake()->word(),
            'code' => fake()->word(),
            'description' => fake()->sentence(),
            'terms' => fake()->sentence(),
            'public' => fake()->boolean(),
            'active_at' => now()->format('Y-m-d\TH:i:s'),
            'expires_at' => now()->format('Y-m-d\TH:i:s'),
        ];

        $response = $this->actingAs($this->manager)->patch(route('admin.coupons.update', $coupon), $data);
        $response->assertStatus(302);

        $updatedCoupon = $coupon->fresh();

        $this->assertSame($updatedCoupon->name, $data['name']);
        $this->assertSame($updatedCoupon->code, $data['code']);
        $this->assertSame($updatedCoupon->description, $data['description']);
        $this->assertSame($updatedCoupon->terms, $data['terms']);
        $this->assertSame($updatedCoupon->public, $data['public']);
        $this->assertSame($updatedCoupon->active_at->format('Y-m-d\TH:i:s'), $data['active_at']);
        $this->assertSame($updatedCoupon->expires_at->format('Y-m-d\TH:i:s'), $data['expires_at']);
    }

    public function test_manager_can_delete_coupon(): void
    {
        $coupon = Coupon::factory()->create();

        $response = $this->actingAs($this->manager)->delete(route('admin.coupons.delete', $coupon));
        $response->assertStatus(302);

        $this->assertSoftDeleted($coupon);
    }

    public function test_manager_can_view_redeem_coupon_form(): void
    {
        $coupon = Coupon::factory()->create();

        $response = $this->actingAs($this->manager)->get(route('admin.coupons.redeem', $coupon));
        $response->assertStatus(200);
        $response->assertSee('Redeem Coupon');
        $response->assertSee('Members');
        $response->assertSee('resource-filter');
    }

    public function test_manager_can_redeem_coupon(): void
    {
        $coupon = Coupon::factory()->create();

        $members = User::factory()->count(3)->create();

        $data['members'] = $members->pluck('id')->toArray();
        $response = $this->actingAs($this->manager)->post(route('admin.coupons.redeem', $coupon), $data);
        $response->assertRedirect(route('admin.coupons.index'));

        $couponRedemptions = CouponRedemption::where('coupon_id', $coupon->id)->get();
        $this->assertSame(
            $couponRedemptions->count(),
            $members->count(),
        );

        foreach ($couponRedemptions as $couponRedemption) {
            $this->assertSame($couponRedemption->coupon->id, $coupon->id);
            $this->assertSame($members->where('id', $couponRedemption->user->id)->count(), 1);
        }
    }

    public function test_manager_can_view_send_coupon_form(): void
    {
        $coupon = Coupon::factory()->create();

        $response = $this->actingAs($this->manager)->get(route('admin.coupons.send', $coupon));
        $response->assertStatus(200);
        $response->assertSee('Send Coupon to Members');
    }

    public function test_manager_can_send_coupons_to_members(): void
    {
        $coupon = Coupon::factory()->create();

        $members = User::factory()->count(3)->create();

        $data['members'] = $members->pluck('id')->toArray();
        $response = $this->actingAs($this->manager)->post(route('admin.coupons.send', $coupon), $data);
        $response->assertRedirect(route('admin.coupons.index'));

        $this->assertSame($coupon->users()->count(), $members->count());
        foreach ($members as $member) {
            $this->assertSame($member->coupons()->value('coupons.id'), $coupon->id);
        }
    }
}
