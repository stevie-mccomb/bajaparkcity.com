<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use App\Models\VerifiedAction;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class VerifiedActionTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->admin()->create();
        $this->manager = User::factory()->manager()->create();
    }

    public function test_managers_can_view_index(): void
    {
        $user = User::factory()->withRole()->create();

        $response = $this->actingAs($user)->get(route('admin.verified-actions.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->manager)->get(route('admin.verified-actions.index'));
        $response->assertStatus(200);
    }

    public function test_index_is_searchable(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();

        $response = $this->actingAs($this->admin)->get(route('admin.verified-actions.index', [
            'search' => $verifiedAction->name,
        ]));

        $response->assertSee($verifiedAction->description);
    }

    public function test_admin_can_view_create_verified_action_form(): void
    {
        $response = $this->actingAs($this->admin)->get(route('admin.verified-actions.create'));
        $response->assertStatus(200);
        $response->assertSee('Create Verified Action');
    }

    public function test_admin_can_create_verified_action(): void
    {
        $verifiedActionData = VerifiedAction::factory()->make()->toArray();

        $response = $this->actingAs($this->admin)->post(route('admin.verified-actions.store'), $verifiedActionData);
        $response->assertStatus(302);

        $this->assertTrue(VerifiedAction::where('name', $verifiedActionData['name'])->exists());
    }

    public function test_admin_can_view_edit_verified_action_form(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();

        $response = $this->actingAs($this->admin)->get(route('admin.verified-actions.edit', $verifiedAction));
        $response->assertStatus(200);
        $response->assertSee($verifiedAction->name);
    }

    public function test_admin_can_update_verified_action(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();
        $newData = VerifiedAction::factory()->make()->toArray();

        $response = $this->actingAs($this->admin)->patch(route('admin.verified-actions.update', $verifiedAction), $newData);
        $response->assertStatus(302);

        $verifiedAction->refresh();
        foreach ($newData as $key => $value) {
            $this->assertSame($verifiedAction->$key, $value);
        }
    }

    public function test_admin_can_delete_verified_action(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();

        $response = $this->actingAs($this->admin)->delete(route('admin.verified-actions.delete', $verifiedAction));
        $response->assertStatus(302);
        $this->assertSoftDeleted($verifiedAction);
    }

    public function test_manager_can_view_verify_form(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();

        $response = $this->actingAs($this->admin)->get(route('admin.verified-actions.verify', $verifiedAction));
        $response->assertStatus(200);
        $response->assertSee('Verify Action');
    }

    public function test_manager_can_verify_action(): void
    {
        $verifiedAction = VerifiedAction::factory()->create();
        $members = User::factory()->count(3)->create();

        $data = [
            'members' => $members->pluck('id')->toArray(),
        ];

        $response = $this->actingAs($this->admin)->post(route('admin.verified-actions.verify.post', $verifiedAction), $data);
        $response->assertStatus(302);

        $this->assertTrue(
            $verifiedAction->users()->count() === $members->count()
        );
    }
}
