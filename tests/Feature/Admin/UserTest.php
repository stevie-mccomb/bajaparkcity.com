<?php

namespace Tests\Feature\Admin;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class UserTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->admin()->create();
    }

    public function test_only_admins_can_view_users_index(): void
    {
        $user = User::factory()->withRole()->create();
        $response = $this->actingAs($user)->get(route('admin.users.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->admin)->get(route('admin.users.index'));
        $response->assertStatus(200);
    }

    public function test_index_is_searchable(): void
    {
        $users = User::factory()->count(3)->create();

        $response = $this->actingAs($this->admin)->get(route('admin.users.index', [
            'search' => $users->first()->email,
        ]));

        $response->assertStatus(200);

        foreach ($users as $key => $user) {
            if ($key === 0) {
                $response->assertSee($user->email);
            } else {
                $response->assertDontSee($user->email);
            }
        }
    }

    public function test_admins_can_view_create_user_form(): void
    {
        $response = $this->actingAs($this->admin)->get(route('admin.users.create'));
        $response->assertStatus(200);
        $response->assertSee('Create User');
    }

    public function test_admin_can_create_user(): void
    {
        $data = User::factory()->make()->toArray();
        unset($data['email_verified_at']);
        $data['new_password'] = Str::random(32);
        $data['new_password_confirmation'] = $data['new_password'];

        $response = $this->actingAs($this->admin)->post(route('admin.users.store'), $data);
        $response->assertStatus(302);

        $user = User::where('email', $data['email'])->first();
        $this->assertModelExists($user);

        $response->assertRedirect(route('admin.users.edit', $user));

        foreach ($data as $key => $value) {
            if (in_array($key, ['email_verified_at', 'new_password', 'new_password_confirmation'])) continue;
            $this->assertSame($user->$key, $value);
        }

        $this->assertTrue(auth()->attempt([ 'email' => $data['email'], 'password' => $data['new_password'] ]));
    }

    public function test_admin_can_view_edit_user_form(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($this->admin)->get(route('admin.users.edit', $user));
        $response->assertStatus(200);
        $response->assertSee('Edit User');
    }

    public function test_admin_can_update_user(): void
    {
        $user = User::factory()->create();

        $data = User::factory()->make()->toArray();
        $data['new_password'] = Str::random(32);
        $data['new_password_confirmation'] = $data['new_password'];

        $response = $this->actingAs($this->admin)->patch(route('admin.users.update', $user), $data);
        $response->assertRedirect(route('admin.users.edit', $user));

        $user->refresh();

        foreach ($data as $key => $value) {
            if (in_array($key, ['email_verified_at', 'new_password', 'new_password_confirmation'])) continue;
            $this->assertSame($user->$key, $value);
        }

        $this->assertTrue(auth()->attempt([ 'email' => $data['email'], 'password' => $data['new_password'] ]));
    }

    public function test_admin_can_delete_user(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($this->admin)->delete(route('admin.users.destroy', $user));
        $response->assertRedirect(route('admin.users.index'));

        $this->assertModelMissing($user);
    }

    public function test_manager_can_view_async_members(): void
    {
        $members = User::factory()->count(3)->create();
        $manager = User::factory()->manager()->create();

        $response = $this->actingAs($manager)->get(route('async.members.index'));
        $response->assertStatus(200);
        foreach ($members as $member) {
            $response->assertSee($member->email);
        }
    }
}
