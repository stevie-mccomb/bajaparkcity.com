<?php

namespace Tests\Feature\Admin\AuditLog;

use App\Models\AuditLogEvent;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class IndexTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create a capable user.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([ 'role_id' => Role::where('slug', 'admin')->value('id') ]);
    }

    public function test_admins_can_view_audit_log(): void
    {
        $user = User::factory()->create([ 'role_id' => Role::factory()->create()->id ]);
        $response = $this->actingAs($user)->get(route('admin.audit-log.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->user)->get(route('admin.audit-log.index'));
        $response->assertStatus(200);
    }

    public function test_audit_log_is_searchable(): void
    {
        $event = AuditLogEvent::factory()->create([
            'event' => 'Example Event',
        ]);

        $otherEvent = AuditLogEvent::factory()->create();

        $response = $this->actingAs($this->user)->get(route('admin.audit-log.index', [ 'search' => 'Example Event' ]));

        $response->assertSee($event->summary);
        $response->assertDontSee($otherEvent->summary);
    }
}
