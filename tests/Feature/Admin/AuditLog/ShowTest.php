<?php

namespace Tests\Feature\Admin\AuditLog;

use App\Models\AuditLogEvent;
use App\Models\Role;
use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class ShowTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Create a capable user.
     */
    public function setUp(): void
    {
        parent::setUp();

        $this->user = User::factory()->create([ 'role_id' => Role::where('slug', 'admin')->value('id') ]);
    }

    public function test_admin_can_view_audit_event(): void
    {
        $auditLogEvent = AuditLogEvent::factory()->create();

        $response = $this->actingAs($this->user)->get(route('admin.audit-log.events.show', $auditLogEvent));
        $response->assertSee($auditLogEvent->summary);
    }
}
