<?php

namespace Tests\Feature;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class HourTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->admin()->create();
        $this->mockHours = '{"time":1692456016916,"blocks":[{"id":"n6lbCiTAEU","type":"paragraph","data":{"text":"Monday: 11:30am - 9:00pm"}}],"version":"2.22.2"}';
    }

    public function test_visitors_can_view_hours(): void
    {
        Storage::fake(config('filesystems.default'));

        Storage::put('hours.json', $this->mockHours);

        $response = $this->get(route('hours'));
        $response->assertStatus(200);
        $response->assertSee('Monday: 11:30am - 9:00pm');
    }

    public function test_only_admins_can_edit_hours(): void
    {
        $user = User::factory()->withRole()->create();
        $response = $this->actingAs($user)->get(route('admin.hours.edit'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->admin)->get(route('admin.hours.edit'));
        $response->assertStatus(200);
        $response->assertSee('Edit Hours');
    }

    public function test_admin_can_update_hours(): void
    {
        Storage::fake(config('filesystems.default'));

        $response = $this->actingAs($this->admin)->patch(route('admin.hours.update'), [
            'hours' => $this->mockHours,
        ]);
        $response->assertRedirect(RouteServiceProvider::HOME);
        $this->assertTrue(Storage::get('hours.json') === $this->mockHours);
    }
}
