<?php

namespace Tests\Feature;

use App\Mail\ContactMessage;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Mail\MailManager;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Str;
use Tests\TestCase;

class ContactTest extends TestCase
{
    public function test_visitor_can_submit_contact_form(): void
    {
        Mail::fake();

        $data = [
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'email' => fake()->safeEmail(),
            'phone' => fake()->phoneNumber(),
            'message' => fake()->sentence(),
            'g-recaptcha-response' => Str::random(32),
        ];

        $response = $this->post(route('contact.send'), $data);
        $response->assertRedirect(route('contact.thank-you'));

        $this->getRealMailable(ContactMessage::class, function (ContactMessage $mail) use ($data) {
            $mail->assertSeeInHtml($data['first_name']);
            $mail->assertSeeInHtml($data['last_name']);
            $mail->assertSeeInHtml($data['email']);
            $mail->assertSeeInHtml($data['phone']);
            $mail->assertSeeInHtml($data['message']);
        });
    }
}
