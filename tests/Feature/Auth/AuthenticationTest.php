<?php

namespace Tests\Feature\Auth;

use Throwable;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Auth\Events\Lockout;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Event;
use Tests\TestCase;

class AuthenticationTest extends TestCase
{
    use RefreshDatabase;

    public function test_login_screen_can_be_rendered()
    {
        $response = $this->get('/login');

        $response->assertStatus(200);
    }

    public function test_users_can_authenticate_using_the_login_screen()
    {
        $user = User::factory()->create();

        $response = $this->post('/login', [
            'email' => $user->email,
            'password' => 'password',
        ]);

        $this->assertAuthenticated();
        $response->assertRedirect(RouteServiceProvider::HOME);
    }

    public function test_users_can_not_authenticate_with_invalid_password()
    {
        $user = User::factory()->create();

        $this->post('/login', [
            'email' => $user->email,
            'password' => 'wrong-password',
        ]);

        $this->assertGuest();
    }

    public function test_users_can_logout(): void
    {
        $user = User::factory()->create();

        $this->actingAs($user)->get(RouteServiceProvider::HOME);
        $this->assertAuthenticated();

        $this->actingAs($user)->post(route('logout'));
        $this->assertGuest();
    }

    public function test_login_rate_limit(): void
    {
        Event::fake();

        for ($i = 0; $i < 10; ++$i) {
            $response = $this->post(route('login'), [
                'email' => 'jdoe@example.com',
                'password' => 'secret',
            ]);
        }

        Event::assertDispatched(Lockout::class);
    }
}
