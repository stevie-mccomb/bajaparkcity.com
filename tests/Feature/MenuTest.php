<?php

namespace Tests\Feature;

use App\Models\Menu;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class MenuTest extends TestCase
{
    use RefreshDatabase;

    public function setUp(): void
    {
        parent::setUp();

        $this->admin = User::factory()->admin()->create();
    }

    public function test_only_admins_can_view_admin_menus_index(): void
    {
        $user = User::factory()->withRole()->create();
        $response = $this->actingAs($user)->get(route('admin.menus.index'));
        $response->assertStatus(403);

        $response = $this->actingAs($this->admin)->get(route('admin.menus.index'));
        $response->assertStatus(200);
    }

    public function test_index_is_searchable(): void
    {
        $menus = Menu::factory()->count(3)->create();

        $response = $this->actingAs($this->admin)->get(route('admin.menus.index', [
            'search' => $menus->first()->name,
        ]));
        $response->assertStatus(200);
        foreach ($menus as $key => $menu) {
            if ($key === 0) {
                $response->assertSee(route('admin.menus.edit', $menu));
            } else {
                $response->assertDontSee(route('admin.menus.edit', $menu));
            }
        }
    }

    public function test_admin_can_view_create_menu_form(): void
    {
        $response = $this->actingAs($this->admin)->get(route('admin.menus.create'));
        $response->assertStatus(200);
        $response->assertSee('Create Menu');
    }

    public function test_admin_can_create_menu(): void
    {
        $data = Menu::factory()->make()->toArray();
        $data['content'] = json_encode($data['content']);

        $response = $this->actingAs($this->admin)->post(route('admin.menus.store'), $data);

        $menu = Menu::where('name', $data['name'])->first();
        $this->assertModelExists($menu);

        $response->assertRedirect(route('admin.menus.edit', $menu));
    }

    public function test_visitor_can_view_menu(): void
    {
        $menu = Menu::factory()->create();

        $route = route('menus.show', [ 'slug' => $menu->slug ]);
        $response = $this->get($route);
        $response->assertStatus(200);
        $response->assertSee($route);
        $response->assertSee($menu->content->sections[0]->items[0]->name);
    }

    public function test_visitor_can_view_dirty_soda_menu(): void
    {
        $menu = Menu::factory()->create([ 'slug' => 'dirty-sodas' ]);

        $route = route('menus.show', [ 'slug' => $menu->slug ]);
        $response = $this->get($route);
        $response->assertStatus(200);
        $response->assertSee('dirty-soda-view');
        $response->assertSee('dirty-soda-menu');
    }

    public function test_admin_can_view_edit_menu_form(): void
    {
        $menu = Menu::factory()->create();

        $response = $this->actingAs($this->admin)->get(route('admin.menus.edit', $menu));
        $response->assertStatus(200);
        $response->assertSee($menu->content->sections[0]->items[0]->name);
    }

    public function test_admin_can_update_menu(): void
    {
        $menu = Menu::factory()->create();
        $data = Menu::factory()->make()->toArray();
        $data['content'] = json_encode($data['content']);

        $response = $this->actingAs($this->admin)->patch(route('admin.menus.update', $menu), $data);
        $response->assertRedirect(route('admin.menus.edit', $menu));

        $menu->refresh();

        foreach ($menu as $key => $value) {
            $this->assertSame($menu->$key, $value);
        }
    }

    public function test_admin_can_delete_menu(): void
    {
        $menu = Menu::factory()->create();

        $response = $this->actingAs($this->admin)->delete(route('admin.menus.destroy', $menu));
        $response->assertRedirect(route('admin.menus.index'));

        $this->assertModelMissing($menu);
    }
}
