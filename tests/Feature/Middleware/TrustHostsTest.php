<?php

namespace Tests\Feature\Middleware;

use App\Http\Middleware\TrustHosts;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class TrustHostsTest extends TestCase
{
    public function test_middleware_will_accept_subdomains_of_primary_domain(): void
    {
        Route::middleware(TrustHosts::class)->get('example', fn () => 'Hello, World!');

        $response = $this->get(str_replace('http://', 'http://example.', config('app.url')) . '/example');
        $response->assertStatus(200);
        $response->assertSee('Hello, World!');
    }
}
