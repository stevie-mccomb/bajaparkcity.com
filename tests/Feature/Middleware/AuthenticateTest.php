<?php

namespace Tests\Feature\Middleware;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class AuthenticateTest extends TestCase
{
    public function test_authenticate_middleware_redirects_guests(): void
    {
        Route::middleware('auth')->get('example', fn () => 'Hello, World!');

        $response = $this->get('example');
        $response->assertStatus(302);
        $response->assertDontSee('Hello, World!');
    }
}
