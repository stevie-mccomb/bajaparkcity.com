<?php

namespace Tests\Feature\Middleware;

use App\Models\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Route;
use Tests\TestCase;

class RedirectIfAuthenticatedTest extends TestCase
{
    public function setUp(): void
    {
        parent::setUp();

        Route::middleware('guest')->get('example', fn () => 'Hello, World!');
    }
    public function test_guest_middleware_redirects_authenticated_users(): void
    {
        $user = User::factory()->create();

        $response = $this->actingAs($user)->get('example');
        $response->assertStatus(302);
        $response->assertDontSee('Hello, World!');
    }

    public function test_guest_middleware_does_not_redirect_guests(): void
    {
        $response = $this->get('example');
        $response->assertStatus(200);
        $response->assertSee('Hello, World!');
    }
}
