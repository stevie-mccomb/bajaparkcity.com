<?php

namespace Tests\Feature;

use App\Mail\NegativeReview;
use App\Mail\NegativeReviewReceived;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Facades\Mail;
use Tests\TestCase;

class ReviewTest extends TestCase
{
    public function test_visitor_can_view_review_screen(): void
    {
        $response = $this->get(route('reviews.create'));
        $response->assertStatus(200);
        $response->assertSee('Leave a Review');
        $response->assertSee('<Review', escape: false);
    }

    public function test_visitor_can_submit_negative_review(): void
    {
        Mail::fake();

        $data = [
            'experience' => 'negative',
            'first_name' => fake()->firstName(),
            'last_name' => fake()->lastName(),
            'email' => fake()->safeEmail(),
            'phone' => fake()->phoneNumber(),
            'when' => fake()->dateTime()->format('Y-m-d H:i:s T'),
            'server' => fake()->name(),
            'spoke_to_manager' => fake()->boolean() ? 'yes' : 'no',
            'manager' => fake()->name(),
            'other_staff_involved' => fake()->boolean() ? 'yes' : 'no',
            'other_staff' => fake()->name(),
            'details' => fake()->sentence(),
        ];

        $response = $this->post(route('reviews.store'), $data);
        $response->assertRedirect(route('reviews.thank-you'));

        $this->getRealMailable(NegativeReview::class, function (NegativeReview $mail) use ($data) {
            foreach ($data as $key => $value) {
                if ($key === 'experience') continue;
                if ($value === 'yes' || $value === 'no') $value = ucfirst($value);
                $mail->assertSeeInHtml($value, escape: false);
            }
        });

        $this->getRealMailable(NegativeReviewReceived::class, function (NegativeReviewReceived $mail) use ($data) {
            foreach ($data as $key => $value) {
                if ($key === 'experience') continue;
                if ($value === 'yes' || $value === 'no') $value = ucfirst($value);
                $mail->assertSeeInHtml($value, escape: false);
            }
        });
    }
}
